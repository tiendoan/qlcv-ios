//
//  Sureportal_Test.swift
//  Sureportal Test
//
//  Created by Tuanhnm on 12/13/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import TaskManagement

class Sureportal_Test: XCTestCase {
  
  // MARK: Process tracking list & submit
  // --------------------------------
  
  func testGetListTrackingForUserNotEmptyOrNil() {
    let expectations = expectation(description: "The response result match the expected results")
    let documentID: String = "2d21bcf3-5665-4f2c-8376-c5d82ccf5a08"
    SyncProvider.getListTrackingForUser(documentID: documentID) { (result, error) in
      XCTAssertNotNil(result, "Result not nil")
      if let res = result {
        debugPrint(res.debugDescription)
        XCTAssertFalse(res.isEmpty, "Should have at least one item")
      }
      expectations.fulfill()
    }
    waitForExpectations(timeout: 30, handler: { (error) in
      if let error = error {
        print("Failed : \(error.localizedDescription)")
      }
    })
  }
  
  func testProcessTrackingDocumentSuccess() {
    let expectations = expectation(description: "The response result match the expected results")
    if let exp: ProcessTracking = ProcessTracking(JSON: [:]) {
      exp.trackingID = "bf2229a2-e347-41b6-954e-1d422119d62c"
      exp.documentID = "2d21bcf3-5665-4f2c-8376-c5d82ccf5a08"
      exp.fromDate = Constants.currentDate
      exp.toDate = Constants.currentDate
      exp.newDueDate = Constants.currentDate
      exp.description = "Bla bla"
      exp.percentFinish = 90
      exp.status = "\(ProcessTrackingStatus.inprogress.rawValue)"
      SyncProvider.processTrackingDocument(tracking: exp, done: { (result, error) in
        if let res = result {
          XCTAssertTrue(res, "Process tracking document success")
        }
        expectations.fulfill()
      })
    }
    waitForExpectations(timeout: 30, handler: { (error) in
      if let error = error {
        print("Failed : \(error.localizedDescription)")
      }
    })
  }
  
  // MARK: Apprise list & submit
  // --------------------------------
  
  func testGetListAppraiseTrackingDocumentNotEmptyOrNil() {
    let expectations = expectation(description: "The response result match the expected results")
    let documentID: String = "ce910fe9-da18-4f63-af1f-c882aa3558ca"
    SyncProvider.getListAppraiseTrackingDocument(documentID: documentID) { (trackings, msg) in
      if let res: [ProcessTracking] = trackings {
        XCTAssertTrue(!res.isEmpty, "Apprise tracking of \(documentID) shouldn't empty.")
      }
      expectations.fulfill()
    }
    waitForExpectations(timeout: 30, handler: { (error) in
      if let error = error {
        print("Failed : \(error.localizedDescription)")
      }
    })
  }
  
  func testGetListTrackingRatingNotEmptyOrNil() {
    let expectations = expectation(description: "The response result match the expected results")
    let trackingID: String = "09bad6fd-9db1-44a7-9586-2bcb07ed0be3"
    SyncProvider.getListTrackingRating(trackingID: trackingID) { (ratings, msg) in
      if let rates: [StatusDocument] = ratings {
        XCTAssertTrue(!rates.isEmpty, "Ratings of tracking \(trackingID) shouldn't empty.")
      }
      expectations.fulfill()
    }
    waitForExpectations(timeout: 30, handler: { (error) in
      if let error = error {
        print("Failed : \(error.localizedDescription)")
      }
    })
  }
  
}
