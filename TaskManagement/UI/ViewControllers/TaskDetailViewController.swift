//
//  TaskDetailViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/3/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class TaskDetailViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var footerView: UIView!
  @IBOutlet weak var footerBottomLayoutConstraint: NSLayoutConstraint!
  @IBOutlet weak var commentTextField: UITextField!
  
  var task: Task?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = task?.title ?? "Dự án tái tổ chức công ty"
    showBack()
    
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.register(withClass: DetailInfoTableViewCell.self)
    tableView.register(withClass: DetailWorkTableViewCell.self)
    tableView.register(withClass: DetailCommentTableViewCell.self)
    
    NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    // Do any additional setup after loading the view.
  }
  
  func keyboardWillHide(_ sender: Notification) {
    if let userInfo = (sender as NSNotification).userInfo {
      if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
        self.footerBottomLayoutConstraint.constant =  0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
      }
    }
  }
  func keyboardWillShow(_ sender: Notification) {
    if let userInfo = (sender as NSNotification).userInfo {
      if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
        self.footerBottomLayoutConstraint.constant = keyboardHeight
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
          self.view.layoutIfNeeded()
        })
      }
    }
  }

  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func actionSendButton(_ sender: Any){
    commentTextField.resignFirstResponder()
  }
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

extension TaskDetailViewController: UITableViewDataSource{
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 1
    case 1:
      return 1
    default:
      return 3
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
    case 0:
      let cell = tableView.dequeueReusableCell(withClass: DetailInfoTableViewCell.self, for: indexPath)
      cell.titleLabel.text = "Xây dựng hệ thống đánh giá năng lực của nhân viên"
      cell.descLabel.text = "Nguyên cứu và xây dựng danh mục vị trí công việc và bô tiêu chí đánh giá năng lực. Tổ chức lấy ý kiến hoàn toàn khách quan từ các thanh viên trong nhóm"
      cell.timeLabel.text = "6 - 16, Th6"
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withClass: DetailWorkTableViewCell.self, for: indexPath)
      cell.avatarImageView.image = #imageLiteral(resourceName: "img_avatar")
      cell.nameLabel.text = "Hoàng Điệp"
      cell.titleLabel.text = "Giám đốc"
      cell.descLabel.text = "Nguyên cứu và xây dựng danh mục vị trí công việc và bô tiêu chí đánh giá năng lực. Tổ chức lấy ý kiến hoàn toàn khách quan từ các thanh viên trong nhóm."
      cell.timeLabel.text = "6-16, Th6, 2017"
      return cell
    default:
      let cell = tableView.dequeueReusableCell(withClass: DetailCommentTableViewCell.self, for: indexPath)
      cell.avatarImageView.image = #imageLiteral(resourceName: "img_avatar")
      cell.nameLabel.text = "Demo"
      cell.timeLabel.text = "1 phút trước"
      cell.contentLabel.text = "Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat."
      return cell
    }
    
  }
}

extension TaskDetailViewController: UITableViewDelegate{
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 1
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView =  DetailHeaderTableView.instanceFromNib()
    switch section {
    case 0:
      headerView.titleLabel.text = "Thông tin"
    case 1:
      headerView.titleLabel.text = "Công việc"
    default:
      headerView.titleLabel.text = "Trao đổi"
    }
    return headerView
  }
}
