//
//  TaskViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/3/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SwipeCellKit

class TaskViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  fileprivate var dataItems = [Task]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.allowsSelection = true
    tableView.allowsMultipleSelectionDuringEditing = true
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 200
    tableView.register(withClass: TaskTableViewCell.self)
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

extension TaskViewController: UITableViewDataSource{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withClass: TaskTableViewCell.self, for: indexPath)
    cell.delegate = self
    cell.avatarImageView.image = #imageLiteral(resourceName: "img_avatar")
    cell.titleLabel.text = "Dự án tái tổ chức công ty"
    cell.descLabel.text = "Xây dưng hệ thống đánh giá năng lực của nhân viên..."
    cell.timeLabel.text = "6-16, Th6, 2017"
    return cell
  }
}

extension TaskViewController: UITableViewDelegate{
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    guard let _ = tableView.cellForRow(at: indexPath) as? TaskTableViewCell else {
      return
    }
    let detailVC = self.storyboard?.instantiateViewController(withClass: TaskDetailViewController.self)
    self.navigationController?.pushViewController(detailVC!)
  }
}

extension TaskViewController: SwipeTableViewCellDelegate {
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
    
    if orientation == .left {
      let aproveAction = SwipeAction(style: .default, title: "Duyệt") { action, indexPath in
        
      }
      aproveAction.image = #imageLiteral(resourceName: "ic_xuly")
      let rateAction = SwipeAction(style: .default, title: "Đánh giá") { action, indexPath in
        
      }
      rateAction.image = #imageLiteral(resourceName: "ic_giaoviec")
      let sendMailAction = SwipeAction(style: .default, title: "Gửi mail") { action, indexPath in
        
      }
      sendMailAction.image = #imageLiteral(resourceName: "ic_chuyen")
      let otherAction = SwipeAction(style: .default, title: "Gửi mail") { action, indexPath in
        
      }
      otherAction.image = #imageLiteral(resourceName: "ic_khac")
      let list = [aproveAction, rateAction, sendMailAction, otherAction]
      for item in list {
        item.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        item.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightLight)
      }
      return list
    }
    else{
      let moveAction = SwipeAction(style: .default, title: "Chuyển vào thư mục") { action, indexPath in
        
      }
      moveAction.image = #imageLiteral(resourceName: "ic_chuyen")
      let deleteAction = SwipeAction(style: .default, title: "Xóa") { action, indexPath in
        
      }
      deleteAction.image = #imageLiteral(resourceName: "ic_chuyen")
      let list = [moveAction, deleteAction]
      for item in list {
        item.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        item.font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightLight)
      }
      return list
    }
  }
  
  func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
    var options = SwipeTableOptions()
    options.expansionStyle = .selection
    options.transitionStyle = .drag
    options.backgroundColor = #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1)
    return options
  }
  
  
}
