//
//  HomeViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/2/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Pageboy
import Tabman
import SwiftIconFont
import LGPlusButtonsView
import SnapKit

class HomeViewController: TabmanViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let mainColor = #colorLiteral(red: 0, green: 0.5215686275, blue: 0.9294117647, alpha: 1)
    self.navigationBar?.shadowImage = UIImage()
    self.navigationBar?.setColors(background: mainColor, text: .white)
    
    //let menuBtn = UIBarButtonItem(image: UIImage.icon(from: .MaterialIcon, iconColor: UIColor.white, code: "menu", imageSize: CGSize(width: 30, height: 30), ofSize: 30), style: .plain, target: self, action: #selector(actionMenuButton))
    let menuBtn = UIBarButtonItem(image: UIImage.icon(from: .MaterialIcon, iconColor: .white, code: "menu", imageSize: CGSize(width: 30, height: 30), ofSize: 30), style: .plain, target: self, action: #selector(actionMenuButton))
    menuBtn.tintColor = UIColor.white
    self.navigationItem.leftBarButtonItem = menuBtn
    
    let logo = UIImageView(image: #imageLiteral(resourceName: "logo"))
    self.navigationItem.titleView = logo
    
    let moreBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_more_vert_white"), style: .plain, target: self, action: #selector(actionMoreButton))
    self.navigationItem.rightBarButtonItems = [moreBtn]
    
    let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    //searchButton.setImage(UIImage.icon(from: .MaterialIcon, iconColor: UIColor.white, code: "search", imageSize: CGSize(width: 25, height: 25), ofSize: 25), for: .normal)
    searchButton.setImage(UIImage.icon(from: .MaterialIcon, iconColor: .white, code: "search", imageSize: CGSize(width: 25, height: 25), ofSize: 25), for: .normal)
    searchButton.backgroundColor = #colorLiteral(red: 0, green: 0.5215686275, blue: 0.9294117647, alpha: 1)
    searchButton.addShadow()
    searchButton.cornerRadius = 15
    self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: searchButton))
    
    self.dataSource = self
    self.bar.appearance = TabmanBar.Appearance({ (appearance) in
      // customise appearance heres
      appearance.style.background = .solid(color: mainColor)
      //appearance.layout.minimumItemWidth = self.view.width/3
      appearance.layout.edgeInset = 0
      appearance.layout.itemVerticalPadding = 10
      appearance.layout.interItemSpacing = 0
      appearance.layout.height = .explicit(value: 20)
//      appearance.interaction.isScrollEnabled = false
      appearance.indicator.color = UIColor.yellow
      appearance.indicator.lineWeight = .normal
      appearance.style.bottomSeparatorColor = mainColor
      appearance.state.color = UIColor.white
      appearance.state.selectedColor = UIColor.white
//      appearance.text.font = 
    })
    
    let button = LGPlusButtonsView(numberOfButtons: 4, firstButtonIsPlusButton: true, showAfterInit: true) { (button, title, title2, index) in
      
    }
    button?.coverColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(0.5)
    button?.setButtonsTitles(["+", "", "", ""], for: .normal)
    button?.setDescriptionsTexts(["", "tạo mới", "báo cáo", "tiện ích"])
    button?.position = .rightBottom
    button?.plusButtonAnimationType = .rotate
    button?.appearingAnimationType = .crossDissolveAndPop
    button?.setButtonsAdjustsImageWhenHighlighted(true)
    button?.setButtonsLayerShadowColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
    button?.setButtonsLayerShadowOpacity(0.5)
    button?.setButtonsLayerShadowRadius(3.0)
    button?.setButtonAt(0, size: CGSize(width: 52, height: 52), for: .all)
    button?.setButtonsLayerShadowOffset(CGSize(width: 0, height: 2))
    button?.setButtonsSize(CGSize(width: 44, height: 44), for: .all)
    button?.setButtonsLayerCornerRadius(22, for: .all)
    button?.setButtonsBackgroundColors([#colorLiteral(red: 0, green: 0.5215686275, blue: 0.9294117647, alpha: 1),#colorLiteral(red: 0, green: 0.5215686275, blue: 0.9294117647, alpha: 1),#colorLiteral(red: 0, green: 0.5215686275, blue: 0.9294117647, alpha: 1),#colorLiteral(red: 0, green: 0.5215686275, blue: 0.9294117647, alpha: 1)], for: .normal)
    button?.setDescriptionsBackgroundColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    button?.setDescriptionsTextColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
    button?.setDescriptionsInsets(UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), for: .all)
    button?.setDescriptionsContentEdgeInsets(UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8), for: .all)
    button?.setDescriptionsLayerCornerRadius(12, for: .all)
    button?.setButtonAt(0, titleFont: UIFont.systemFont(ofSize: 25), for: .all)
    view.addSubview(button!)
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func actionMenuButton(){
    self.main()?.toggleMenu()
  }
  
  func actionMoreButton(){
    
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

// MARK: - PageboyViewControllerDataSource
extension HomeViewController: PageboyViewControllerDataSource{
  
  func viewControllers(forPageboyViewController pageboyViewController: PageboyViewController) -> [UIViewController]? {
    // return array of view controllers
    let allVC = self.storyboard?.instantiateViewController(withClass: TaskViewController.self)
    let inVC = self.storyboard?.instantiateViewController(withClass: TaskViewController.self)
    let expritedVC = self.storyboard?.instantiateViewController(withClass: TaskViewController.self)
    let viewControllers = [allVC, inVC, expritedVC]
    
    // configure the bar
    self.bar.items = [Item(title: "TẤT CẢ"),
                      Item(title: "TRONG HẠN"),
                      Item(title: "QUÁ HẠN")]
    

    
    return viewControllers as? [UIViewController]
  }
  
  func defaultPageIndex(forPageboyViewController pageboyViewController: PageboyViewController) -> PageboyViewController.PageIndex? {
    // use default index
    return nil
  }
}
