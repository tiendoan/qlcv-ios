//
//  LeftMenuViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 8/2/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class LeftMenuViewController: UITableViewController {
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var countLabel: UILabel!
  @IBOutlet weak var commentLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    avatarImageView.image = #imageLiteral(resourceName: "img_avatar")
    nameLabel.text = "Nguyen Minh"
    titleLabel.text = "Giám đốc"
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
}
