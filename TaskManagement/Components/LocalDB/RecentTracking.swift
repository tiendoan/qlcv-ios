//
//  RecentTracking.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 10/16/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class RecentTracking : Mappable {
  var id: String?
  var assignToJobTitle: String?
  var assignToLoginName: String?
  var assignToName: String?
  var assignToPicture: String?
  var percentFinish: Float?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    id <- map["ID"]
    assignToJobTitle <- map["AssignToJobTitle"]
    assignToLoginName <- map["AssignToLoginName"]
    assignToName <- map["AssignToName"]
    assignToPicture <- map["AssignToPicture"]
    percentFinish <- map["PercentFinish"]
  }
  
  
}

