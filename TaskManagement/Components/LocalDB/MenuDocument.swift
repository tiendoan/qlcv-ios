//
//  MenuDocument.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 10/5/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB

struct MenuDocumentPair : RowConvertible {
  let menu: Menu
  let document: Document
  
  init(row: Row) {
    // Those scopes are defined by the all() method below
    menu = Menu(row: row.scoped(on: "menu")!)
    document = Document(row: row.scoped(on: "document")!)
  }
  
  static func all(menuId: String = "") -> AdaptedTypedRequest<AnyTypedRequest<MenuDocumentPair>> {
    var query = """
            SELECT menu.*, document.*
            FROM menu
            JOIN menu_document ON menu.ID = menu_document.MenuID
            JOIN document ON document.ID = menu_document.DocID
            WHERE menu.ID = ?
            ORDER BY document.DocDate DESC
            """
    if menuId == "Home" {   //TODO: need check more this case
        query = """
        SELECT document.*, menu_document.MenuID
        FROM document
        JOIN menu_document ON document.ID = menu_document.DocID
        WHERE menu_document.MenuID = ?
        ORDER BY document.DocDate DESC
        """
        
        return SQLRequest(query, arguments: [menuId])
            .asRequest(of: MenuDocumentPair.self)
            .adapted { db in
                try ScopeAdapter([
                    "document": SuffixRowAdapter(fromIndex: 0),
                    "menu": SuffixRowAdapter(fromIndex: db.columnCount(in: "document"))])
        }
    }
    
    return SQLRequest(query, arguments: [menuId])
        .asRequest(of: MenuDocumentPair.self)
        .adapted { db in
            try ScopeAdapter([
                "menu": SuffixRowAdapter(fromIndex: 0),
                "document": SuffixRowAdapter(fromIndex: db.columnCount(in: "menu"))])
    }
  }
  
  static func fetchAll(_ db: Database, menuId: String) throws -> [MenuDocumentPair] {
    return try all(menuId: menuId).fetchAll(db)
  }
}

//for pair in try BookAuthorPair.fetchAll(db) {
//  print("\(pair.book.title) by \(pair.author.name)")
//}

