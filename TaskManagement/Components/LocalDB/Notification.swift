//
//  Notification.swift
//  TaskManagement
//
//  Created by Tuanhnm on 1/9/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class DocumentNotification: Mappable {
  
  var authorID: String?
  var authorJobTitle: String?
  var authorLoginName: String?
  var authorName: String?
  var authorPicture: String?
  var created: Date?
  var itemID: String?
  var itemIcon: String?
  var itemType: String?
  var itemTypeName: String?
  var summary: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    authorID <- map["AuthorID"]
    authorJobTitle <- map["AuthorJobTitle"]
    authorLoginName <- map["AuthorLoginName"]
    authorName <- map["AuthorName"]
    authorPicture <- map["AuthorPicture"]
    created <- (map["Created"], CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    itemID <- map["ItemID"]
    itemIcon <- map["ItemIcon"]
    itemType <- map["ItemType"]
    itemTypeName <- map["ItemTypeName"]
    summary <- map["summary"]
  }
}
