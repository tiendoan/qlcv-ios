//
//  Menu.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/27/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import GRDB
import ObjectMapper

enum MenuCode: String {
    case currentTasks = "CVHT"          // Công Việc Hiện Tại
    case new = "MOI_CVCT"               // Mới
    case processing = "DXL_CVCT"        // Đang Xứ Lý
}

class Menu: Record, Mappable {
  var id: String?
  var parentId: String?
  var name: String?
  var code: String?
  var count: Int?
  var icon: String?
  var order: Int?
  var childrens: [Menu]?
  var createdAt: Date?
  var updatedAt: Date?
  
  required init?(map: Map) {
    super.init()
  }
  
  func mapping(map: Map) {
    id <- map["ID"]
    parentId <- map["ParentID"]
    name <- map["Name"]
    code <- map["Code"]
    count <- map["Count"]
    icon <- map["IconLink"]
    order <- map["NoOrder"]
    childrens <- map["Childrens"]
  }
  
  // MARK: Record overrides
  
  override class var databaseTableName: String {
    return "menu"
  }
  
  required init(row: Row) {
    id = row.value(named: "ID")
    parentId = row.value(named: "ParentID")
    name = row.value(named: "Name")
    code = row.value(named: "Code")
    count = row.value(named: "Count")
    icon = row.value(named: "IconLink")
    order = row.value(named: "NoOrder")
    createdAt = row.value(named: "CreatedAt")
    order = row.value(named: "UpdatedAt")
    super.init(row: row)
  }
  
  override func encode(to container: inout PersistenceContainer) {
    container["ID"] = id
    container["ParentID"] = parentId
    container["name"] = name
    container["Code"] = code
    container["Count"] = count
    container["IconLink"] = icon
    container["NoOrder"] = order
    container["CreatedAt"] = createdAt
    container["UpdatedAt"] = updatedAt
  }
  
}

/*
 class DBMenu: Record {
 var id: Int64?
 var name: String
 var createdAt: Date
 var updatedAt: Date?
 
 
 init(name: String) {
 self.name = name
 self.createdAt = Date()
 self.updatedAt = nil
 super.init()
 }
 
 // MARK: Record overrides
 
 override class var databaseTableName: String {
 return "menu"
 }
 
 required init(row: Row) {
 id = row.value(named: "id")
 name = row.value(named: "name")
 createdAt = row.value(named: "created_at")
 updatedAt = row.value(named: "updated_at")
 super.init(row: row)
 }
 
 override func encode(to container: inout PersistenceContainer) {
 container["id"] = id
 container["name"] = name
 container["created_at"] = createdAt
 container["updated_at"] = updatedAt
 }
 
 override func didInsert(with rowID: Int64, for column: String?) {
 id = rowID
 }
 
 }*/
