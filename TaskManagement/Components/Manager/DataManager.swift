//
//  DataManager.swift
//  TaskManagement
//
//  Created by Scor Doan on 4/13/18.
//  Copyright © 2018 Thang Nguyen. All rights reserved.
//

import Foundation

class DataManager {
    static let shared = DataManager()
    
    var allUsers: [UserProfile] = [UserProfile]()
    var flatEmployees: [Assignee] = [Assignee]()
    var employees: [Assignee] = [Assignee]() {
        didSet {
            departments = getDepartments(list: employees)//flatEmployees.filter {$0.isDept == true}
        }
    }
    var departments: [Assignee] = [Assignee]()
    
    func getAllUsersInfo() {
        SyncProvider.getAllUsersInfo { (users) in
            self.allUsers = users ?? []
        }
    }
    func getAllEmployees () {
        SyncProvider.getAllUsersInDepartments { (assigners) in
            if let lAssigners: [Assignee] = assigners {
                self.employees = lAssigners
                self.flatEmployees = Constants.default.recursiveFlatmap(list: lAssigners, true).sorted{ ($0.value ?? "") < ($1.value ?? "") }
            } else {
                self.employees = Dummies.shared.getAllEmployees()
                self.flatEmployees = Dummies.shared.getAllEmployees()
            }
        }
    }
    
    func getDepartments(list: [Assignee]) -> [Assignee] {
        var results = [Assignee]()
        for element in list {
            if element.isDept == true {
                results.append(element)
            }
            if let subList: [Assignee] = element.childrens,
                !subList.isEmpty {
                results += getDepartments(list: subList)
            }
        }
        return results
    }
    
    class func finishDocument(document:Document, done: @escaping (Bool) -> ()) {
        guard let docID = document.id, let deptID = document.editDepartment else {
            done(false)
            return
        }
        let docIDs = [docID]
        let deptIDs = [deptID]
        SyncProvider.finishDocuments(docIDs: docIDs, deptIDs: deptIDs) { (finish) in
            //TODO: need to process with database
            //....
            InstanceDB.default.finishDocument(document: document)
            NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_FINISHED_DOCUMENT), object: nil, userInfo: nil)
            done(finish)
        }
    }
    
    //Using for future
    class func finishDocuments(docs:[Document], done: @escaping (Bool) -> ()) {
        let docIDs = docs.filter{$0.id != nil}.map{$0.id!}
        let deptIDs = docs.filter{$0.editDepartment != nil}.map{$0.editDepartment!}
        SyncProvider.finishDocuments(docIDs: docIDs, deptIDs: deptIDs) { (finish) in
            //TODO: need to process with database
            //....
            done(finish)
        }
    }
}
