//
//  Dummies.swift
//  TaskManagement
//
//  Created by Tuanhnm on 11/29/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class Dummies {
  static let shared = Dummies()
  
  typealias Token2CombineModel = (key: String, value: String, type: RolesInTask)
  
  func getAllCategories() -> [CategoryTracking] {
    var categories: [CategoryTracking] = [CategoryTracking]()
    if let category: CategoryTracking = Mapper<CategoryTracking>().map(JSONString: "{\"ID\":\"b0b354ea-e1e5-ca2f-698a-1ff8111f6f75\",\"Name\":\"Khác\"}") {
      categories.append(category)
    }
    if let category: CategoryTracking = Mapper<CategoryTracking>().map(JSONString: "{\"ID\":\"0e645e66-644b-0813-16ee-84fa17643d62\",\"Name\":\"Tài chính\"}") {
      categories.append(category)
    }
    if let category: CategoryTracking = Mapper<CategoryTracking>().map(JSONString: "{\"ID\":\"a3595cb8-fcf6-a3e6-fe08-269cf73b8ed5\",\"Name\":\"Kế toán\"}") {
      categories.append(category)
    }
    if let category: CategoryTracking = Mapper<CategoryTracking>().map(JSONString: "{\"ID\":\"47b44766-dc9e-2e4f-56fc-1f3bafc83062\",\"Name\":\"Nhân sự\"}") {
      categories.append(category)
    }
    if let category: CategoryTracking = Mapper<CategoryTracking>().map(JSONString: "{\"ID\":\"28ed9b4f-3f7d-5007-f31a-5f5964758125\",\"Name\":\"Hành chánh\"}") {
      categories.append(category)
    }
    if let category: CategoryTracking = Mapper<CategoryTracking>().map(JSONString: "{\"ID\":\"a3a3ac93-e8a5-b7d3-b986-dd948bf7cfb5\",\"Name\":\"CNTT\"}") {
      categories.append(category)
    }    
    return categories
  }
  
  func getAllEmployees() -> [Assignee] {
    var employees: [Assignee] = [Assignee]()    
    if let assignee: Assignee = Mapper<Assignee>().map(JSONString: "{\"Admin\":null,\"Childrens\":null,\"Code\":null,\"Icon\":\"/StyleBioPortalV3/Images/icoPerson.png\",\"IsChecked\":false,\"IsDept\":false,\"IsVisible\":true,\"JobTitle\":\"Phó Tổng Giám đốc\",\"ParentID\":\"4d0d31b5-2cac-4062-9d03-d39aad2316af\",\"ParentName\":null,\"Picture\":\"/images/Avatar/photonggiamdoc.jpg\",\"Text\":\"Phó tổng giám đốc\",\"Value\":\"hps2013\\photonggiamdoc\"}") {
      employees.append(assignee)
    }
    if let assignee: Assignee = Mapper<Assignee>().map(JSONString: "{\"Admin\":null,\"Childrens\":null,\"Code\":null,\"Icon\":\"/StyleBioPortalV3/Images/icoPerson.png\",\"IsChecked\":false,\"IsDept\":false,\"IsVisible\":true,\"JobTitle\":\"Giám đốc\",\"ParentID\":\"4d0d31b5-2cac-4062-9d03-d39aad2316af\",\"ParentName\":null,\"Picture\":\"/images/Avatar/giamdoc.png\",\"Text\":\"Giám đốc\",\"Value\":\"hps2013\\giamdoc\"}") {
      employees.append(assignee)
    }
    if let assignee: Assignee = Mapper<Assignee>().map(JSONString: "{\"Admin\":null,\"Childrens\":null,\"Code\":null,\"Icon\":\"/StyleBioPortalV3/Images/icoPerson.png\",\"IsChecked\":false,\"IsDept\":false,\"IsVisible\":true,\"JobTitle\":\"Tổng Giám đốc\",\"ParentID\":\"4d0d31b5-2cac-4062-9d03-d39aad2316af\",\"ParentName\":null,\"Picture\":\"/images/Avatar/tonggiamdoc.jpg\",\"Text\":\"Tổng giám đốc\",\"Value\":\"hps2013\\tonggiamdoc\"}") {
      employees.append(assignee)
    }
    if let assignee: Assignee = Mapper<Assignee>().map(JSONString: "{\"Admin\":null,\"Childrens\":null,\"Code\":null,\"Icon\":\"/StyleBioPortalV3/Images/icoPerson.png\",\"IsChecked\":false,\"IsDept\":false,\"IsVisible\":true,\"JobTitle\":\"Trợ lý TGĐ\",\"ParentID\":\"4d0d31b5-2cac-4062-9d03-d39aad2316af\",\"ParentName\":null,\"Picture\":\"/images/Avatar/troly.png\",\"Text\":\"Trợ lý TGD\",\"Value\":\"hps2013\\troly\"}") {
      employees.append(assignee)
    }
    if let assignee: Assignee = Mapper<Assignee>().map(JSONString: "{\"Admin\":null,\"Childrens\":null,\"Code\":null,\"Icon\":\"/StyleBioPortalV3/Images/icoPerson.png\",\"IsChecked\":false,\"IsDept\":false,\"IsVisible\":true,\"JobTitle\":\"Phó Phòng\",\"ParentID\":\"01119eae-30e1-4b05-905e-52453ff0187c\",\"ParentName\":null,\"Picture\":\"/images/Avatar/phophongcntt.jpg\",\"Text\":\"Phó phòng CNTT\",\"Value\":\"hps2013\\phophongcntt\"}") {
      employees.append(assignee)
    }
    return employees
  }
}
