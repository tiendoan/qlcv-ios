//
//  Router.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
  
    static var baseURLString:String {
        get {
            return UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "http://sureportal.bioportal.vn"
        }
    }
  
    case getKey()
  case login(username:String, password:String)
  case sendCode(emailOrMobileNumber: String)
  case verify(code:String)
  case resetPass(password: String, code:String)
  case logout()
  case updateDeviceInfo(deviceToken: String, deviceName: String, imei: String, os: String, osVersion: String)
  case removeDeviceInfo(deviceToken: String)
  
  case getAllUsersDepartments(deptID: String, mode: Int, keyword: String)
  case getAllUsersInfo()
  case getCategorizeTracking(keyWord: String)
  case menu(parentId: String, treeFilterCode: String)
  case document(treefilterID:String, page:Int)
  
  case detail(id: String)
  case documentProcessHistory(editDeptID: String, documentID: String)
    case documentTracking(deptID: String, status: Int, viewType: Int, documentID: String, isChild: Bool)
  case documentRecentTracking(deptID: String, top: Int, documentID: String)
  case updateStatusDocument(documentID: String)
  case documentTranfer(documentID: String)
  case getAssignersForTracking(documentID: String)
  
  case approveDocument(documentID: String, workFlowStepID: String, contentObj: String)
  case assignTrackingDocument(params: Parameters)
  case returnDocument(documentID: String, comment: String)
  case updateAssignTrackingInfoDocument(params: Parameters)
  case finishDocuments(docIDs: [String], deptIDs: [String])
    
  case getListTrackingForUser(documentID: String)
  case processTrackingDocument(document: Parameters)
  
  case getListAppraiseTrackingDocument(documentID: String)
  case getListTrackingRating(trackingID: String)
  case appriseTrackingDocument(apprise: Parameters)
  
  case topNewItem(itemType: String, page:Int)
  case chartCountOverview(itemType: String)
  case chartLineOverview(viewType:String)
  
    case advancedSearchDocument(keyWord: String, advancedParams: [String], page: Int)
  case getListNotification()
    case updateNotification(itemId: String, itemType: String)
  case uploadFile(UploadFileInfo)
    case getListTrackingWorkflowDocument(deptID: String, documentID: String)
    case sendMail(documentID: String, subject: String, body: String, receivers: String)
  var method: HTTPMethod {
    switch self {
    default:
      return .post
    }
  }
  
  var path: String {
    switch self {
      
    case .getKey:
        return "mobileapis/SurePortalServices.svc/GetEncryptKey"
    case .login:
      return "mobileapis/SurePortalServices.svc/LoginAccount"
    case .sendCode:
      return "mobileapis/SurePortalServices.svc/SendCodeResetPasswordAccount"
    case .verify:
      return "mobileapis/SurePortalServices.svc/VerifyCodeResetPasswordAccount"
    case .resetPass:
      return "mobileapis/SurePortalServices.svc/ResetPasswordAccount"
    case .logout:
      return "mobileapis/SurePortalServices.svc/LogoutAccount"
    case .updateDeviceInfo:
      return "mobileapis/SurePortalServices.svc/UpdateDeviceInfo"
    case .removeDeviceInfo:
      return "mobileapis/SurePortalServices.svc/RemoveDeviceInfo"
      
    case .getAllUsersDepartments:
      return "/mobileapis/SurePortalServices.svc/GetListFilterTreeUserDepartment"
    case .getAllUsersInfo:
        return "/mobileapis/SurePortalServices.svc/GetAllUserInfo"
    case .getCategorizeTracking:
      return "mobileapis/SurePortalServices.svc/GetListCategorizeTracking"
    case .document:
      return "mobileapis/SurePortalServices.svc/GetListFilterDocument"
    case .menu:
      return "mobileapis/SurePortalServices.svc/GetListTreeFilterDocument"
      
    case .detail:
      return "mobileapis/SurePortalServices.svc/GetDocument"
    case .documentProcessHistory:
      return "mobileapis/SurePortalServices.svc/GetListProcessHistoryDocument"
    case .documentTracking(_, _, _, _, let isChild):
        
        return isChild ? "/mobileapis/SurePortalServices.svc/GetListChildTrackingDocument" : "mobileapis/SurePortalServices.svc/GetListTrackingDocument"
    case .updateStatusDocument:
      return "mobileapis/SurePortalServices.svc/UpdateStatusDocument"
    case .documentRecentTracking:
      return "mobileapis/SurePortalServices.svc/GetListRecentTrackingDocument"
    case .documentTranfer:
      return "mobileapis/SurePortalServices.svc/GetListTransferViewDocument"
    case .getAssignersForTracking:
      return "mobileapis/SurePortalServices.svc/GetAssignTrackingInfoDocument"
      
    case .approveDocument:
      return "mobileapis/SurePortalServices.svc/ApproveDocument"
    case .assignTrackingDocument:
      return "mobileapis/SurePortalServices.svc/AssignTrackingDocument"
    case .updateAssignTrackingInfoDocument:
      return "mobileapis/SurePortalServices.svc/UpdateAssignTrackingInfoDocument"
    case .finishDocuments:
        return "mobileapis/SurePortalServices.svc/FinishDocument"
    case .returnDocument:
      return "mobileapis/SurePortalServices.svc/ReturnDocument"
      
    case .getListTrackingForUser:
      return "mobileapis/SurePortalServices.svc/GetListTrackingForUser"
    case .processTrackingDocument:
      return "mobileapis/SurePortalServices.svc/ProcessTrackingDocument"
    
    case .getListAppraiseTrackingDocument:
      return "mobileapis/SurePortalServices.svc/GetListAppraiseTrackingDocument"
    case .getListTrackingRating:
      return "mobileapis/SurePortalServices.svc/GetListTrackingRating"
    case .appriseTrackingDocument:
      return "mobileapis/SurePortalServices.svc/AppraiseTrackingDocument"
      
    case .topNewItem:
      return "mobileapis/SurePortalServices.svc/GetListTopNewDocument"
    case .chartCountOverview:
      return "mobileapis/SurePortalServices.svc/GetListChartCountDashBoard"
    case .chartLineOverview:
      return "mobileapis/SurePortalServices.svc/GetListChartLineDashBoard"
      
    case .advancedSearchDocument:
      return "mobileapis/SurePortalServices.svc/AdvancedSearchDocument"
    case .getListNotification:
      return "mobileapis/SurePortalServices.svc/GetListNotification"
    case .updateNotification:
        return "mobileapis/SurePortalServices.svc/UpdateNotification"
    case .uploadFile:
        return "mobileapis/SurePortalServices.svc/UploadFile"
    case .getListTrackingWorkflowDocument:
        return "mobileapis/SurePortalServices.svc/GetListTrackingWorkflowDocument"
    case .sendMail:
        return "mobileapis/SurePortalServices.svc/SendMail"
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let result: (path: String, parameters: Parameters) = {
      switch self {
      case .getKey():
        var secretKey = ""
        if let udidDevice: UUID = UIDevice.current.identifierForVendor {
            secretKey += udidDevice.uuidString
        } else {
            secretKey += "XXX-XXX"
        }
        let dateTime = Date().toFormatDate(format: "yyyy_MM_dd_HH_mm")
        secretKey += "_\(dateTime)"
        return (self.path, ["secretKey":secretKey])
      case let .login(username,password):
        return (self.path, ["username":username, "password":password])
      case let .sendCode(emailOrMobileNumber):
        return (self.path, ["emailOrMobileNumber": emailOrMobileNumber])
      case let .verify(code):
        return (self.path, ["code":code])
      case let .resetPass(password, code):
        return (self.path, ["password": password, "code": code])
      case .logout():
        return (self.path, [:])
      case let .updateDeviceInfo(deviceToken, deviceName, imei, os, osVersion):
        return (self.path, ["token": deviceToken, "name": deviceName, "imei": imei, "osName": os, "osVersion": osVersion])
      case let .removeDeviceInfo(deviceToken):
        return (self.path, ["token": deviceToken])
        
      case .getAllUsersDepartments(let deptID, let mode, let keyword):
        return (self.path, ["deptID": deptID, "mode": mode, "keyword": keyword])
      case .getAllUsersInfo():
        return (self.path, [:])
      case .getCategorizeTracking(let keyWord):
        return (self.path, ["keyWord": keyWord])
      case .document(let treefilterID, let page):
        return (self.path, ["keyword": "", "treefilterID": treefilterID, "page": page])
      case let .menu(parentId, treeFilterCode) where treeFilterCode != "":
        return (self.path, ["parentID": parentId, "treeFilterCode": treeFilterCode, "userdelegate":""])
      case let .menu(parentId, _):
        return (self.path, ["parentID": parentId])
      case let .detail(documentId):
        return (self.path, ["documentID": documentId])
      case let .updateStatusDocument(documentId):
        return (self.path, ["documentID": documentId])
        
      case let .topNewItem(itemType, page):
        return (self.path, ["itemType": itemType, "page": page])
      case let .chartCountOverview(itemType):
        return (self.path, ["itemType": itemType])
      case let .chartLineOverview(viewType):
        return (self.path, ["viewType": viewType])
        
      case let .documentProcessHistory(editDeptID, documentID):
        return (self.path, ["editDeptID": editDeptID, "documentID": documentID])
      case let .documentTracking(deptID, status, viewType, documentID, isChild):
        if isChild {
            return (self.path, ["deptID": deptID, "status": status, "viewType":viewType, "parentID": documentID])
        } else {
            return (self.path, ["deptID": deptID, "status": status, "viewType":viewType, "documentID": documentID])
        }
        
      case let .documentRecentTracking(deptID, top, documentID):
        return (self.path, ["deptID": deptID, "top": top, "documentID": documentID])
      case let .documentTranfer(documentID):
        return (self.path, ["documentID": documentID])
      case let .getAssignersForTracking(documentID):
        return (self.path, ["documentID": documentID])
        
      case let .approveDocument(documentID, workFlowStepID, contentObj):
        return (self.path, ["documentID": documentID, "workFlowStepID": workFlowStepID, "contentObj": contentObj])
      case let .assignTrackingDocument(params):
        return (self.path, params)
      case let .returnDocument(documentID, comment):
        return (self.path, ["documentID": documentID, "comment": comment])
      case let .updateAssignTrackingInfoDocument(params):
        return (self.path, params)
      case let .finishDocuments(docIDs, deptIDs):
        return (self.path, ["lstDocID": docIDs, "lstDeptID":deptIDs])
      case let .getListTrackingForUser(documentID):
        return (self.path, ["documentID": documentID])
      case let .processTrackingDocument(document):
        return (self.path, ["processTracking": document])
      case let .getListAppraiseTrackingDocument(documentID):
        return (self.path, ["documentID": documentID])
      case let .getListTrackingRating(trackingID):
        return (self.path, ["trackingID": trackingID])
      case let .appriseTrackingDocument(document):
        return (self.path, ["appraiseTracking": document])
      
        
      case let .advancedSearchDocument(keyword, advancedParams, page):
        
        //let advancedParams = ["OrgID|", "DocDateFrom|2018-01-01", "DocDateTo|2018-12-31", "Category|All"]
        var params = ["keyword": keyword, "page": page] as [String : Any]
         params["lstParams"] = advancedParams
        
         return (self.path, params)
      case .getListNotification():
        return (self.path, [:])
      
      case .updateNotification(let itemId, let itemType):
        return (self.path, ["itemID":itemId, "itemType": itemType])
      //Upload file
      case .uploadFile(let fileInfo):
        let params = ["fileExt": fileInfo.type!, "fileContentBase64":fileInfo.data64, "offset":fileInfo.offset, "length":fileInfo.size, "isEndFile":false] as [String : Any]
        return (self.path, params)
      case .sendMail(let documentID, let subject, let body, let receivers):
        let params = ["documentID": documentID, "subject": subject, "body": body, "receivers": receivers] as [String : String]
        return (self.path, params)
        
      case let .getListTrackingWorkflowDocument(editDeptID, documentID):
        return (self.path, ["deptID": editDeptID, "trackingDocumentID": documentID])
        }
        
    }()
    
    let url = try Router.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
    urlRequest.httpMethod = method.rawValue
    urlRequest.setValue("com.pvgas", forHTTPHeaderField: "User-Agent")
    debugPrint("Request: \(urlRequest.url)")
    debugPrint("Params: \(result.parameters)")
    switch self {
    default:
      urlRequest = try JSONEncoding.default.encode(urlRequest, with: result.parameters)
    }
    
    return urlRequest
  }
}
