//
//  SyncProvider.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

class SyncProvider {
  
  //MARK: - Menu
  /// get menu data
  ///
  /// - Parameters:
  ///   - parentId: id of parent
  ///   - done: return escaping with result
  static func getMenu(parentId: String = "00000000-0000-0000-0000-000000000000", treeFilterCode: String = "", done:@escaping (_ result: [Menu]) -> ()) {
    let router = Router.menu(parentId: parentId, treeFilterCode: treeFilterCode)
    request(router)
      .validate()
      .responseObject {  (response : DataResponse<MenuResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data ?? [])
          } else {
            done([])
          }
        case .failure(let error):
          print(error)
          done([])
        }
    }
  }
  
  ///MARK: - Assigners of tasks
  /// Get list assigners for documents
  ///
  /// - Parameters:
  ///   - documentID: document identify.
  ///   - done: return
  static func getAssignersByDocument(documentID: String, done: @escaping(_ result: [AssignTracking]?) -> ()) {
    let router = Router.getAssignersForTracking(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response : DataResponse<AssignTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
    }
  }
  
  //MARK: - Document
  /// get list document for tree id
  ///
  /// - Parameters:
  ///   - treefilterID: id of tree to get
  ///   - page: index of page
  ///   - done: done: return escaping with result
  static func getDocument(treefilterID: String, page: Int,done:@escaping (_ result: DocumentData?) -> ()) {
    let router = Router.document(treefilterID: treefilterID, page: page)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<DocumentResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
    }
  }
  
  
  /// get list progress history for document
  ///
  /// - Parameters:
  ///   - editDeptID: id of dept
  ///   - documentID: id of document
  ///   - done: return escaping with result
  static func getListProcessHistoryDocument(editDeptID: String, documentID:String, done:@escaping (_ result: [ProgressHistory]?) -> ()) {
    let router = Router.documentProcessHistory(editDeptID: editDeptID, documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ProgressHistoryResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
    
    /// get List TrackingWorkflowDocument
    ///
    /// - Parameters:
    ///   - deptID: id of dept
    ///   - trackingDocumentID: id of document
    ///   - done: return escaping with result
    static func getListTrackingWorkflowDocument(deptID: String, trackingDocumentID:String, done:@escaping (_ result: [ProgressHistory]?) -> ()) {
        let router = Router.getListTrackingWorkflowDocument(deptID: deptID, documentID: trackingDocumentID)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject {  (response : DataResponse<ProgressHistoryResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    print(error)
                    done(nil)
                }
            }.responseDecrypted { (response) in
                print(response.value ?? "")
        }
    }
  
  
  /// get list tracking for document
  ///
  /// - Parameters:
  ///   - deptID: id of dept
  ///   - status:  "-1" = "Tất cả", "0" = "Mới", "1" = "Đang xử lý", "2" = "Báo cáo", "3" = "Hoãn xử lý", "4" = "Kết thúc"
  ///   - viewType: "0" = "Tất cả", "1" = "Phòng ban", "2" = "Cá nhân"
  ///   - documentID: id of document
  ///   - done: return escaping with result
    static func getListTrackingDocument(deptID: String, status: Int, viewType: Int, documentID:String, isChild: Bool = false, done:@escaping (_ result: [ProgressTracking]?) -> ()) {
        let router = Router.documentTracking(deptID: deptID, status: status, viewType: viewType, documentID: documentID, isChild: isChild)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ProgressTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
  
  
  /// get detail of a document
  ///
  /// - Parameters:
  ///   - documentId: id of document
  ///   - done: done: return escaping document full detail
  static func getDetail(documentId: String, done:@escaping (_ result: Document?) -> ()){
    let router = Router.detail(id: documentId)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<DetailResponse>) in
        if let data = response.result.value {
          done(data.data)
        } else {
          done(nil)
        }
    }
  }
  
  //MARK: - Home
  /// get top new item of home dashboard
  ///
  /// - Parameters:
  ///   - itemType: itemType, ex: "Document"
  ///   - page: current page
  ///   - done: done: return escaping with result
  static func getTopNewItemHomeDashboard(itemType: String, page:Int, done:@escaping (_ result: [Document]?) -> ()){
    let router = Router.topNewItem(itemType: itemType, page:page)
    request(router)
      .responseObject {  (response : DataResponse<DocumentResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            if(data.data != nil && data.data!.items != nil){
              done(data.data!.items!)
            }
            else{
              done(nil)
            }
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
        
    }
  }
  
  /// get chart count of home dashboard
  ///
  /// - Parameters:
  ///   - itemType: itemType, ex: "Document"
  ///   - done: done: return escaping with result
  static func getChartCountOverviewHomeDashboard(itemType: String,done:@escaping (_ result: ChartData?) -> ()){
    let router = Router.chartCountOverview(itemType: itemType)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ChartData>) in
        if let chartData = response.result.value {
            done(chartData)
        } else {
            done(nil)
        }
    }
  }
  
  
  /// get chart line of home dashboard
  ///
  /// - Parameters:
  ///   - viewType: viewType, ex: "month"
  ///   - done: done: return escaping with result
  static func getChartLineOverviewHomeDashboard(viewType: String, done:@escaping (_ result: ChartData?) -> ()){
    let router = Router.chartLineOverview(viewType: viewType)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<ChartData>) in
        if let chartData = response.result.value {
          done(chartData)
        } else {
          done(nil)
        }
    }
  }
  
  
  /// update status a document
  ///
  /// - Parameters:
  ///   - documentID: id of document
  ///   - done: done: return escaping with true or flase
  static func updateStatusDocument(documentID:String, done:@escaping(_ success:Bool) -> ()){
    let router = Router.updateStatusDocument(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<UpdateStatusDocumentResponse>) in
        if let data = response.result.value {
          done(data.data)
        } else {
          done(false)
        }
    }
  }
  
  static func downloadFilePDF(path:String, done:@escaping(_ result:URL?) -> ()){
    
    let url = Router.baseURLString + path
    let destination = DownloadRequest.suggestedDownloadDestination()
    
    download(url, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
      print("Progress: \(progress.fractionCompleted)")
      } .validate().responseData { ( response ) in
        //print(response.destinationURL!.absoluteString)
        done(response.destinationURL)
    }
  }
  
  static func getListRecentTrackingDocument(deptID: String, top: Int, documentID:String, done:@escaping (_ result: [RecentTracking]?) -> ()) {
    let router = Router.documentRecentTracking(deptID: deptID, top: top, documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<RecentTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
  
  static func getListTranferDocument(documentID:String, done:@escaping (_ result: [Transfer]?) -> ()) {
    let router = Router.documentTranfer(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject {  (response : DataResponse<TranferResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
    
    static func getAllUsersInfo(done: @escaping(_ result: [UserProfile]?) -> ()) {
        let router = Router.getAllUsersInfo()
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (repsones: DataResponse<UsersResponse>) in
                switch repsones.result {
                case .success:
                    if let data = repsones.result.value {
                        done(data.data)
                    } else {
                        done(nil)
                    }
                case .failure(let error):
                    debugPrint("\(error.localizedDescription)")
                    done(nil)
                }
            }.responseDecrypted { (repsones) in
                print(repsones.value ?? "")
        }
    }
  
  static func getAllUsersInDepartments(done: @escaping(_ result: [Assignee]?) -> ()) {
    let router = Router.getAllUsersDepartments(deptID: Constants.default.DEFAULT_PARENT_ID, mode: 1, keyword: "")
    request(router)      
      .validate(statusCode: 200..<300)
      .responseObject { (repsones: DataResponse<AssigneeResponse>) in
        switch repsones.result {
        case .success:
          if let data = repsones.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          debugPrint("\(error.localizedDescription)")
          done(nil)
        }
      }.responseDecrypted { (repsones) in
        print(repsones.value ?? "")
    }
  }
  
  static func getCategoryTracking(keyWord: String, done: @escaping(_ result: [CategoryTracking]?) -> ()) {
    let router = Router.getCategorizeTracking(keyWord: keyWord)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<CategoriesResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done(nil)
          }
        case .failure(let error):
          print(error)
          done(nil)
        }
      }.responseDecrypted { (response) in
        print(response.value ?? "")
    }
  }
  
  static func approveDocument(documentID: String, workFlowStepID: String, contentObj: String, done: @escaping() -> ()) {
    let router = Router.approveDocument(documentID: documentID, workFlowStepID: workFlowStepID, contentObj: contentObj)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            print("approveDocument: \(String(describing: data.message))")
            done()
          } else {
            done()
          }
        case .failure(let error):
          print(error)
          done()
        }
    }
  }
  
  static func assignTrackingDocument(params: AssignTrackingRequest, done: @escaping() -> ()) {    
    let router = Router.assignTrackingDocument(params: params.toJSON())
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            print("assignTrackingDocument: \(String(describing: data.message))")
            done()
          } else {
            done()
          }
        case .failure(let error):
          print(error)
          done()
        }
    }
  }
    
    static func finishDocuments(docIDs: [String], deptIDs: [String], done: @escaping(Bool) -> ()) {
        let router = Router.finishDocuments(docIDs: docIDs, deptIDs: deptIDs)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        print("finishDocuments: \(String(describing: data.message))")
                        done(true)
                    } else {
                        done(false)
                    }
                case .failure(let error):
                    print(error)
                    done(false)
                }
        }
    }
  
  static func returnDocument(documentID: String, comment: String, done: @escaping() -> ()) {
    let router = Router.returnDocument(documentID: documentID, comment: comment)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<BaseResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            print("\(String(describing: data.message))")
            done()
          } else {
            done()
          }
        case .failure(let error):
          print(error)
          done()
        }
    }
  }
    
    static func sendEmail(documentID: String, subject: String, body: String, receivers: String, done: @escaping(_ result: Bool?, _ error: String?) -> ()) {
        let router = Router.sendMail(documentID: documentID, subject: subject, body: body, receivers: receivers)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<BaseResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value, let status = data.status {
                        if status == 1 {    //success
                            done(true, nil)
                        } else {
                            done(false, data.message)
                        }
                    } else {
                        done(false, nil)
                    }
                case .failure(let error):
                    print(error)
                    done(false, error.localizedDescription)
                }
        }
    }
  
  static func getListTrackingForUser(documentID: String, done: @escaping(_ result: [ProcessTracking]?, _ error: String?) -> ()) {
    let router = Router.getListTrackingForUser(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<ProcessTrackingResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data, nil)
          }
        case .failure(let error):
          debugPrint(error)
          done(nil, error.localizedDescription)
        }
    }
  }
  
  static func processTrackingDocument(tracking: ProcessTracking, done: @escaping(_ result: Bool?, _ error: String?) -> ()) {
    let router = Router.processTrackingDocument(document: tracking.toJSON())
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<VerifyCodeResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value, let status = data.status {
            if status == 1 {    //success
                done(true, nil)
            } else {
                done(false, data.message)
            }
          } else {
            done(false, nil)
          }
        case .failure(let error):
          done(false, error.localizedDescription)
        }
    }
  }
  
  static func getListAppraiseTrackingDocument(documentID: String, done: @escaping(_ trackings: [ProcessTracking]?, _ message: String?) -> ()) {
    let router = Router.getListAppraiseTrackingDocument(documentID: documentID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<ProcessTrackingResponse>) in
        switch response.result {
        case .success:
          if let data: ProcessTrackingResponse = response.result.value {
            done(data.data, data.message)
          }
        case .failure(let error):
          debugPrint(error)
          done(nil, error.localizedDescription)
        }
    }
  }
  
  static func getListTrackingRating(trackingID: String, done: @escaping(_ ratings: [StatusDocument]?, _ msg: String? ) -> ()) {
    let router = Router.getListTrackingRating(trackingID: trackingID)
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<TrackingRatingResponse>) in
        switch response.result {
        case .success:
          if let data: TrackingRatingResponse = response.result.value {
            done(data.data, data.message)
          }
        case .failure(let error):
          debugPrint(error)
          done(nil, error.localizedDescription)
        }
    }
  }
  
    static func advancedSearchDocument(keyword: String, advancedParams: [String], page:Int, done:@escaping (_ result: [Document]) -> ()){
        let router = Router.advancedSearchDocument(keyWord: keyword, advancedParams: advancedParams, page: page)
    request(router)
      .responseObject {  (response : DataResponse<DocumentResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            if(data.data != nil && data.data!.items != nil){
              done(data.data!.items!)
            }
            else{
              done([])
            }
          } else {
            done([])
          }
        case .failure(let error):
          debugPrint(error)
          done([])
        }        
    }
  }
  
  static func getListNotification(done:@escaping (_ result: [NotificationItem]?) -> ()){
    let router = Router.getListNotification()
    request(router)
      .responseObject {  (response : DataResponse<NotificationResponse>) in
        switch response.result {
        case .success:
          if let data = response.result.value {
            done(data.data)
          } else {
            done([])
          }
        case .failure(let error):
          debugPrint(error)
          done([])
        }
    }
  }
    
    static func updateNotification(itemID: String, itemType: String, done: @escaping (_ result: Bool?, _ error: String?) -> ()){
        let router = Router.updateNotification(itemId: itemID, itemType: itemType)
        request(router)
            .responseObject {  (response : DataResponse<VerifyCodeResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value, let status = data.status {
                        if status == 1 {    //success
                            done(true, nil)
                        } else {
                            done(false, data.message)
                        }
                    } else {
                        done(false, nil)
                    }
                case .failure(let error):
                    debugPrint(error)
                    done(false, error.localizedDescription)
                }
        }
    }
  
  static func appriseTrackingDocument(tracking: ProcessTracking, done: @escaping(_ result: Bool?, _ error: String?) -> ()) {
    let router = Router.appriseTrackingDocument(apprise: tracking.toJSON())
    request(router)
      .validate(statusCode: 200..<300)
      .responseObject { (response: DataResponse<VerifyCodeResponse>) in
        switch response.result {
        case .success:
            if let data = response.result.value, let status = data.status {
                if status == 1 {    //success
                    done(true, nil)
                } else {
                    done(false, data.message)
                }
            } else {
                done(false, nil)
            }
        case .failure(let error):
          done(false, error.localizedDescription)
        }
    }
  }
    
    static func uploadFile(_ uploadParams:UploadFileInfo, updateHandler:@escaping (_ percent:Float) -> Void ,done: @escaping (_ fileID: String?, _ error: String?) -> Void){
        let router = Router.uploadFile(uploadParams)
        request(router)
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<UploadFileResponse>) in
                switch response.result {
                case .success:
                    if let data = response.result.value,
                        let value = data.fileID {
                        done(value, nil)
                    } else {
                        done(nil, nil)
                    }
                case .failure(let error):
                    done(nil, error.localizedDescription)
                }
        }
        
    }
  
}
