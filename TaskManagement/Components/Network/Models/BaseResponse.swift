//
//  BaseResponse.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponse: Mappable {
  var status: Int?
  var message: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    status <- map["Status"]
    message <- map["Message"]
  }
}

class UserProfileResponse: BaseResponse {
    var data:UserProfile?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class GetKeyResponse: BaseResponse {
    var data:String?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["d"]
    }
}

class SendCodeResponse: BaseResponse {
    var data:Int?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class VerifyCodeResponse: BaseResponse {
    var data:Bool?
  
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class UploadFileResponse: BaseResponse{
    var fileID:String?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        fileID <- map["Data"]
    }
}

class ResetPassResponse: BaseResponse{
    var data:Int?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class MenuResponse: BaseResponse {
  var data: [Menu]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class DocumentResponse: BaseResponse {
    var data:DocumentData?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class DetailResponse: BaseResponse {
  var data:Document?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class ChartResponse: BaseResponse {
    var data:[Chart]?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}


class ProgressHistoryResponse: BaseResponse {
  var data:[ProgressHistory]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class ProgressTrackingResponse: BaseResponse {
  var data:[ProgressTracking]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class UpdateStatusDocumentResponse: BaseResponse {
    var data = false
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class RecentTrackingResponse: BaseResponse {
  var data:[RecentTracking]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class UsersResponse: BaseResponse {
    var data: [UserProfile]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["Data"]
    }
}

class AssigneeResponse: BaseResponse {
  var data: [Assignee]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class CategoriesResponse: BaseResponse {
  var data:[CategoryTracking]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class TranferResponse: BaseResponse {
  var data:[Transfer]?
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class AssignTrackingResponse: BaseResponse {
  var data: [AssignTracking]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class ProcessTrackingResponse: BaseResponse {
  var data: [ProcessTracking]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
}

class TrackingRatingResponse: BaseResponse {
  
  var data: [StatusDocument]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
  
}

class NotificationResponse: BaseResponse {
  
  var data: [NotificationItem]?
  
  required init?(map: Map) {
    super.init(map: map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map: map)
    data <- map["Data"]
  }
  
}

