//
//  Comment.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/21/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment: Mappable {
  var id: String?
  var comment: String?
  var avatar: String?
  var createdAt: Date?
  var updatedAt: Date?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    
  }
}
