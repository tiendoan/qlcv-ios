//
//  Employee.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/21/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

class Employee: Mappable {
  var id: String?
  var name: String?
  var title: String?
  var roomName: String?
  var avatar: String?
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    
  }
}
