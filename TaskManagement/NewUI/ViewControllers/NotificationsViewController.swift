//
//  NotificationsViewController.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import ObjectMapper
import SwipeCellKit
import SVProgressHUD

class NotificationsViewController: UIViewController {
  
  @IBOutlet weak var notificationsTableView: UITableView!
  
  let PAGE_TITLE: String = "Thông báo"
  let HIDE_KB_TITLE: String = "Ẩn bàn phím"
  let DATA_EMPTY: String = "Không có thông báo nào !"
  
  var notifications: [NotificationItem] = [NotificationItem] ()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupComponents()
    self.getNotificationsFromApi()
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UIApplication.shared.applicationIconBadgeNumber != self.notifications.count {
            self.getNotificationsFromApi()
        }
    }
  
  fileprivate func setupComponents() {
    
    IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
    IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = HIDE_KB_TITLE
    
    self.setTitleNavigationBar(PAGE_TITLE)
    self.setupDefaultNavigationBar()
    
    self.showBack()
    
    notificationsTableView.separatorStyle = .none
    notificationsTableView.estimatedRowHeight = 70
    notificationsTableView.rowHeight = UITableViewAutomaticDimension
    notificationsTableView.register(withClass: NotificationTableCell.self)
    
  }
}

// MARKS: get notifications from apis
// ---------------------------
extension NotificationsViewController {
  
  fileprivate func getNotificationsFromApi() {
    SVProgressHUD.show()
    SyncProvider.getListNotification(done: { (result) in
      if let rs: [NotificationItem] = result {
        self.notifications = rs
      } else {
        self.notifications = []
      }
        self.notificationsTableView.reloadData()
        SVProgressHUD.dismiss()
        UIApplication.shared.applicationIconBadgeNumber = self.notifications.count
        NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
    })
  }
  
}

extension NotificationsViewController: UITableViewDelegate {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if !self.notifications.isEmpty {
      TableViewHelper.EmptyMessage(message: "", viewController: self, tableView: self.notificationsTableView)
      return 1
    } else {
      TableViewHelper.EmptyMessage(message: DATA_EMPTY, viewController: self, tableView: self.notificationsTableView)
      return 0
    }
  }
  
}

extension NotificationsViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return notifications.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let notification: NotificationItem = self.notifications[safe: indexPath.row] else {
      return UITableViewCell()
    }
    let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell", for: indexPath) as! NotificationTableCell
    cell.delegate = self
    cell.config(notification)
    return cell
  }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let notification: NotificationItem = self.notifications[safe: indexPath.row] else {
            return
        }
        let documentData: DocumentNotification = Mapper<DocumentNotification>().map(JSON:[String : AnyObject]())!
        // Go to detail.
        let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        documentData.itemID = notification.itemID
        documentData.itemType = notification.itemType
        detailVC.documentFromNotification = documentData
        self.navigationController?.pushViewController(detailVC)
    }
}

extension NotificationsViewController: SwipeTableViewCellDelegate {
    
    func updateReadNotificationDocument(indexPath:IndexPath, itemID: String, itemType: String) {
        SVProgressHUD.show()
        SyncProvider.updateNotification(itemID: itemID, itemType: itemType) { (result, error) in
            SVProgressHUD.dismiss()
            if let success = result, success == true {
                self.notifications.remove(at: indexPath.row)
                self.notificationsTableView.beginUpdates()
                self.notificationsTableView.deleteRows(at: [indexPath], with: .right)
                self.notificationsTableView.endUpdates()
                let badge = UIApplication.shared.applicationIconBadgeNumber
                UIApplication.shared.applicationIconBadgeNumber = badge > 0 ? badge-1 : 0
                NotificationCenter.default.post(name: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, userInfo: nil)
            }

        }
    }
    
    func tableView (_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        if orientation == .right {
            guard let notification: NotificationItem = self.notifications[safe: indexPath.row] else {
                return []
            }

            let action = SwipeAction(style: .destructive, title: "Delete", handler: { (action, index) in
                self.updateReadNotificationDocument(indexPath: indexPath, itemID: notification.itemID!, itemType: notification.itemType!)
            })
            return [action]
        }
        return []
    }
    
    func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return options
    }
    
}

