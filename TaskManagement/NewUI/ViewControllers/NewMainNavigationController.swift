//
//  NewMainNavigationController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class NewMainNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
