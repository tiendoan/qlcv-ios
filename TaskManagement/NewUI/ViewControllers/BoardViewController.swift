//
//  BoardViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import EBCardCollectionViewLayout
import SVProgressHUD
import DropDown

class BoardViewController: UIViewController {
  
  @IBOutlet fileprivate weak var collectionView:UICollectionView!
  @IBOutlet fileprivate weak var pageControl:UIPageControl!
    
    @IBOutlet weak var subMenuButton: UIButton!
    var menus:[Menu] = []
    let taskDropDown: DropDown = DropDown()
    var needReload = false
    var selectedMenuIdx = 0
    
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(withClass: BoardCollectionCell.self)
    if let layout = self.collectionView.collectionViewLayout as? EBCardCollectionViewLayout{
      //layout.offset = UIOffset(horizontal: 20, vertical: 0)
      layout.layoutType = .horizontal
    }
    //self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    self.collectionView.isScrollEnabled = false
    self.pageControl.numberOfPages = 0

}
    func setupDropdownMenu() {
        if let name = menus.first?.name {
            self.subMenuButton.setTitle("\(name)   ▼", for: .normal)
        } else {
            self.subMenuButton.setTitle("--   ▼", for: .normal)
        }
        
        
        taskDropDown.anchorView = subMenuButton
        taskDropDown.width = 200
        taskDropDown.bottomOffset = CGPoint(x: 0, y: subMenuButton.bounds.height + 10)
        taskDropDown.isMultipleTouchEnabled = false
        taskDropDown.textColor = UIColor.gray
        taskDropDown.textFont = Theme.default.semiBoldFont(size: 13)
        taskDropDown.cellNib = UINib(nibName: "CustomDropdownCell", bundle: nil)
        
        taskDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CustomDropdownCell else { return }
            
            // Setup your custom UI components
            cell.iconImageView.image = #imageLiteral(resourceName: "ic_timeline_circle")
        }
        
        taskDropDown.selectionAction = { [unowned self] (index, item) in
            self.subMenuButton.setTitle("\(item)   ▼", for: .normal)
            self.collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: false)
            self.pageControl.currentPage = index
            self.selectedMenuIdx = index
        }
        
        // dropdown datasource
        
        taskDropDown.dataSource = menus.map {
            let count = $0.count ?? 0
            if let uName: String = $0.name {
                return count > 0 ? uName + " [\(count)]" : uName
            } else {
                return "Chưa xác định"
            }
        }
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if needReload {
            collectionView.reloadData()
            needReload = false
        }
    }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
    
    @IBAction func selectMenuDropDown(_ sender: Any) {
        taskDropDown.show()
    }
  
    func setupMenus(_ menus: [Menu],_ selectItem: Int, updateMenuOnly: Bool = false) {
    self.menus.removeAll()
    self.menus.append(contentsOf: menus)
    self.setupDropdownMenu()
        guard menus.count > 0 else { return }
        let selectedMenu = menus[updateMenuOnly ? selectedMenuIdx : selectItem]
    let name = selectedMenu.name ?? "-"
    let count = selectedMenu.count ?? 0
    let title = count > 0 ? "\(name)[\(count)]   ▼" : "\(name)   ▼"
        
    if updateMenuOnly {
        self.subMenuButton.setTitle(title, for: .normal)
        return
    }
    if(self.collectionView != nil){
      self.collectionView.isHidden = true
        self.subMenuButton.setTitle(title, for: .normal)
      self.collectionView.reloadData {
        self.collectionView.scrollToItem(at: IndexPath(row: selectItem, section: 0), at: .centeredHorizontally, animated: false)
        self.pageControl.currentPage = selectItem
        self.collectionView.isHidden = false
      }
      self.pageControl.numberOfPages = self.menus.count
    }
  }
  
  func setupMenus(_ selectItem: Int) {
    if(self.collectionView != nil){
      self.collectionView.reloadData {
        self.collectionView.scrollToItem(at: IndexPath(row: selectItem, section: 0), at: .centeredHorizontally, animated: false)
        self.pageControl.currentPage = selectItem
      }
      self.pageControl.numberOfPages = self.menus.count
    }
  }
  
}

// MARK: Define extensions
// ---------------------------

extension BoardViewController: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return menus.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BoardCollectionCell", for: indexPath) as! BoardCollectionCell
    cell.config(menus[indexPath.row])
    cell.indexItem = indexPath
    cell.delegate = self
    
    return cell
  }
    
    
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    if let layout = self.collectionView.collectionViewLayout as? EBCardCollectionViewLayout{
      pageControl.currentPage = layout.currentPage
    }
  }
}

extension BoardViewController: BoardCollectionCellDelegate {
    func taskCellDidSelected(document: Document?, itemIndex: IndexPath?, rowIndex: IndexPath?) {
        let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
        detailVC.document = document
        detailVC.didFinishDocument = { [weak self] in
            self?.needReload = true
        }
        detailVC.didupdatedDocument = { [weak self] in
            if let indexPath = itemIndex {
                self?.collectionView.reloadItems(at: [indexPath])
            }
            
        }
        self.navigationController?.pushViewController(detailVC)
        InstanceDB.default.updateStatusDocument(documentID: document?.id ?? "", done: { (result) in
        })
    }

  
  func finishTaskProcess(document: Document?, index: IndexPath?) {
    self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
  }
  
  func assignTaskProcess(document: Document?, index: IndexPath?) {
    let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    assignVC.document = document
    assignVC.delegate = self
    assignVC.indexPath = index
    assignVC.getAssigners()
    self.showViewController(viewController: assignVC, false)
  }
  
  func approveTaskProcess(document: Document?, index: IndexPath?) {
    let approveVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    approveVC.document = document
    approveVC.indexItem = index
    approveVC.delegate = self
    self.showViewController(viewController: approveVC, false)
  }
  
  func processTaskProcess(document: Document?, index: IndexPath?) {
    let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    processVC.delegate = self
    processVC.indexPath = index
    processVC.document = document
    self.showViewController(viewController: processVC, false)
  }
  
  func appriseTaskProcess(document: Document?, index: IndexPath?) {
    let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    appriseVC.delegate = self
    appriseVC.indexPath = index
    appriseVC.document = document
    appriseVC.indexPath = index
    self.showViewController(viewController: appriseVC, false)
  }
  
  func emailTaskProcess(document: Document?, index: IndexPath?) {
    let emailVC: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    emailVC.document = document
    emailVC.delegate = self
    self.showViewController(viewController: emailVC, false)
  }
  
  func taskCellAttachSelected(document: Document?, rowIndex: IndexPath?) {
    if(document != nil && document!.fileDocuments != nil && document!.fileDocuments!.count == 1){
      let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
      vc?.fileDoucment = document!.fileDocuments![0]
      vc?.document = document!
      vc?.indexPath = rowIndex
      vc?.delegate = self
      self.present(vc!, animated: true, completion: nil)
    } else {
      let vc: ListFileAttachViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      vc.modalTransitionStyle = .crossDissolve
      vc.modalPresentationStyle =  .overCurrentContext
      vc.files = document?.fileDocuments ?? []
      vc.document = document!
      self.navigationController?.present(vc, animated: true, completion: nil)
    }
  }
  
  func actionHistoryProcess(document: Document?) {
    let assginVC: TaskHistoryProcessViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    let nav = UINavigationController(rootViewController: assginVC)
    nav.isNavigationBarHidden = true
    nav.modalPresentationStyle = .overCurrentContext
    self.present(nav, animated: false, completion: nil)
  }
  
  func showAllProcess(document: Document?, index: IndexPath?) {
    guard let selectedDocument: Document = document else {
      return
    }
    let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
    for i in 0..<selectedDocument.actions!.count {
      let action: UIAlertAction = UIAlertAction(title: selectedDocument.actions![i].actionName, style: .default) { (_) in
        if let actionType: DocumentAction = selectedDocument.actions![i].actionType {
          if actionType == .approve {
            let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            assignVC.document = selectedDocument
            assignVC.indexItem = index
            assignVC.delegate = self
            self.showViewController(viewController: assignVC, false)
          } else if actionType == .assign {
            let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            assignVC.delegate = self
            assignVC.document = selectedDocument
            assignVC.getAssigners()
            self.showViewController(viewController: assignVC, false)
          } else if actionType == .process {
            let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            processVC.delegate = self
            self.showViewController(viewController: processVC, false)
          } else if actionType == .appraise {
            let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            appriseVC.delegate = self
            appriseVC.document = document
            appriseVC.indexPath = index
            self.showViewController(viewController: appriseVC, false)
          } else if actionType == .sendMail {
            let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
            sendEmailCV.document = document
            sendEmailCV.delegate = self
            self.showViewController(viewController: sendEmailCV, false)
          } else if actionType == .finish {
            self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
          }
        }
      }
      actionSheet.addAction(action)
    }
    let cancel: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
    }
    actionSheet.addAction(cancel)
    self.present(actionSheet, animated: true, completion: nil)
  }
  
}

extension BoardViewController: ProcessTaskViewDelegate {
  
  func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
    if status {
      var msg = "Xử lý thành công !"
      if let msgTemp: String = message,
        msgTemp != "" {
        msg = msgTemp
      }
      self.view.makeToast(msg)
      if let selectedIndex: IndexPath = index {
        self.collectionView.reloadItems(at: [selectedIndex])
      }
    } else {
      self.view.makeToast("Có lỗi xảy ra, chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !")
    }
  }
  
  
  func closeView() {
  }
  
}

extension BoardViewController: AppriseTaskViewDelegate {
  
  func appriseCloseView() {
  }
  
  func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
    self.view.makeToast("Xử lý thành công !")
  }
  
}

extension BoardViewController: SendEmailViewDelegate {
  
  func sendEmailCloseView() {
    
  }
  
  func sendEmailDoneView() {
    self.view.makeToast("Gửi mail thành công !")
  }
  
}

extension BoardViewController: TaskAssignNewViewDelegate {
  
  func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
    var defaultMsg: String = "Xử lý thành công !"
    if let confirmMsg: String = approveMsg {
      defaultMsg = confirmMsg
    } else if let backMsg: String = returnMsg {
      defaultMsg = backMsg
    } else if let failMsg: String = errorMsg {
      defaultMsg = failMsg
    }
    self.view.makeToast(defaultMsg)
    if let selectedIndex: IndexPath = index {
      self.collectionView.reloadItems(at: [selectedIndex])
    }
  }
}

extension BoardViewController: TaskAssignNoneApproveDelegate {
  
  func onCloseView(status: Bool, indexPath: IndexPath?) {
    if status {
      self.view.makeToast("Xử lý thành công !")
      if let index = indexPath {
        self.collectionView.reloadItems(at: [index])
      }
    } else {
      self.view.makeToast("Có lỗi xảy ra !")
    }
  }
  
}

extension BoardViewController: AttachmentDetailViewDelegate {
    func doneView(rowIndex: IndexPath?, action: DocumentAction) {
        if action != .sendMail, let selectedIndex: IndexPath = rowIndex {
            self.collectionView.reloadItems(at: [selectedIndex])
        }
    }
  
}

