//
//  SettingViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/9/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Kingfisher

class SettingViewController: UIViewController {

    @IBOutlet fileprivate weak var btExit:UIButton!
    
    @IBOutlet fileprivate weak var imgAvatar:UIImageView!
    @IBOutlet fileprivate weak var lbName:UILabel!
    @IBOutlet fileprivate weak var lbPosition:UILabel!
    @IBOutlet fileprivate weak var lbEmail:UILabel!
    
    
    @IBOutlet fileprivate weak var lbVersion:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .init(rawValue: 0)
        lbVersion.text = "Sure portal @ 2017 build \(Constants.getVersionName()) (\(Constants.getVersionBuild()))"
        
        setupUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func setupUser(){
        imgAvatar.contentMode = .scaleAspectFill
        lbName.text = CurrentUser?.fullName ?? ""
        lbPosition.text = CurrentUser?.jobTitle ?? "Chưa xác định"
        lbEmail.text = CurrentUser?.email ?? ""
        
        let domain = UserDefaults.standard.string(forKey: kSettingDomainAddress) ?? "http://sureportal.bioportal.vn"
        let url = URL(string: domain + (CurrentUser?.picture ?? ""))
        if url != nil{
            self.imgAvatar.kf.setImage(with: url!, placeholder: UIImage(color: UIColor.lightGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}

extension SettingViewController{
    @IBAction func pressesExit(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func pressesChangePassword(_ sender: UIButton){
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withClass: ChangePasswordViewController.self)
        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func pressesChangeAvatar(_ sender: UIButton){
        self.view.endEditing(true)
        chooseImage()
    }
}
extension SettingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func chooseImage(){
        
        let vc = UIImagePickerController()
        vc.delegate = self
        vc.allowsEditing = false
        vc.sourceType = .photoLibrary
        self.present(vc, animated: true) {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgAvatar.image = image
        } else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
