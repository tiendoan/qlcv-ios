//
//  DashboardViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwipeCellKit

class DashboardViewController: UIViewController {
  
  fileprivate let showItem = 5
  
    @IBOutlet weak var vwContentView: UIView!
    @IBOutlet fileprivate weak var tableView:UITableView!
  @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
  
  var topNewDocument:[Document] = []
  fileprivate var chartCount:[Chart] = []
  fileprivate var chartLine:[Chart] = []
  fileprivate var isLoadTopNew = false
  fileprivate var isLoadChartCount = false
  fileprivate var isLoadChartLine = false
  
  fileprivate var isViewMore = false
  fileprivate var numberItemTop = 0
  fileprivate var viewType:ViewType = .month
  var needReload = false
    
  override func viewDidLoad() {
    super.viewDidLoad()
    numberItemTop = showItem
    vwContentView.layer.masksToBounds = true
    tableView.separatorStyle = .none
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = UITableViewAutomaticDimension
    tableView.register(withClass: TaskTableCell.self)
    tableView.register(withClass: DashboardReportTableCell.self)
    tableView.register(withClass: PieChartTableCell.self)
    tableView.register(withClass: LineChartTableCell.self)
    getData()
    let refresh = UIRefreshControl()
    refresh.tintColor = UIColor.white
    refresh.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    tableView.addSubview(refresh)
    //let longPressGesture = UILongPressGestureRecognizer(target: self, action: (#selector(handleLongPress(_:))))
    //self.tableView.addGestureRecognizer(longPressGesture)
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if needReload {
            tableView.reloadData()
        }
    }
  
  func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
    let point = gestureRecognizer.location(in: self.tableView)
    let indexPath = self.tableView.indexPathForRow(at: point)
    if indexPath != nil{
      if gestureRecognizer.state == .ended{
        print("Section \(indexPath!.section) row \(indexPath!.row)")
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
    func reloadItemHomeDashboard() {
        InstanceDB.default.getTopNewItemHomeDashboard { (result) in
            self.topNewDocument.removeAll()
            self.topNewDocument.append(contentsOf: result)
            self.isLoadTopNew = true
            self.reloadData()
        }
    }
    
  func getData() {
    SVProgressHUD.show()
    tableView.isHidden = true
    InstanceDB.default.getTopNewItemHomeDashboard { (result) in
      self.topNewDocument.append(contentsOf: result)
      self.isLoadTopNew = true
      self.reloadData()
    }
    
    InstanceDB.default.getChartCountOverviewHomeDashboard { (result) in
        self.chartCount.removeAll()
      self.chartCount.append(contentsOf: result)
      self.isLoadChartCount = true
      self.reloadData()
    }
    
    InstanceDB.default.getChartLineOverviewHomeDashboard(viewType: self.viewType.rawValue) { (result) in
      self.chartLine.append(contentsOf: result)
      self.isLoadChartLine = true
      self.reloadData()
    }
  }
  
  func reloadData(){
    if (isLoadTopNew && isLoadChartCount && isLoadChartLine) {
      SVProgressHUD.dismiss()
      self.tableView.reloadData()
      tableView.isHidden = false
    }
  }
  
    func finishDocument(document: Document, indexPath:IndexPath) {
        SVProgressHUD.show()
        DataManager.finishDocument(document: document, done: { (finish) in
            SVProgressHUD.dismiss()
            if finish {
                self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                self.tableView.reloadRows(at: [indexPath], with: .none)
            } else {
                self.view.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
            }
        })
    }
  func refreshData(_ sender:UIRefreshControl){
    
    var isTop = false
    var isPie = false
    var isLine = false
    
    InstanceDB.default.getTopNewItemHomeDashboard(isPull: true) { (result) in
      self.topNewDocument.removeAll()
      self.topNewDocument.append(contentsOf: result)
      self.tableView.reloadData()
      isTop = true
      if (isTop && isPie && isLine) {
        sender.endRefreshing()
      }
    }
    
    
    InstanceDB.default.getChartCountOverviewHomeDashboard(isPull: true) { (result) in
      self.chartCount.removeAll()
      self.chartCount.append(contentsOf: result)
      self.tableView.reloadData()
      isPie = true
      if (isTop && isPie && isLine) {
        sender.endRefreshing()
      }
    }
    
    
    InstanceDB.default.getChartLineOverviewHomeDashboard (isPull: true,viewType: self.viewType.rawValue){ (result) in
      self.chartLine.removeAll()
      self.chartLine.append(contentsOf: result)
      self.tableView.reloadData()
      isLine = true
      if (isTop && isPie && isLine) {
        sender.endRefreshing()
      }
    }
  }
  
    
}

extension DashboardViewController: UITableViewDataSource, UITableViewDelegate, LineChartCellDelegate{
  
  func pressesBTViewMore(_ sender: UIButton) {
    if (self.isViewMore == false) {
      if(topNewDocument.count > numberItemTop+showItem){
        numberItemTop = numberItemTop + showItem
      } else {
        numberItemTop = topNewDocument.count
        self.isViewMore = true
      }
    } else {
      numberItemTop = showItem
      isViewMore = false
    }
    tableView.reloadData()
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      if (topNewDocument.count < numberItemTop) {
        return topNewDocument.count
      }
      return numberItemTop
    } else {
      return 1
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 {
      return 30
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if (indexPath.section == 0) {
      return UITableViewAutomaticDimension
    }
    return 350
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if section == 0 {
      let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 31))
      label.backgroundColor = UIColor.white
      label.text = "  Văn bản mới"
      label.font = Theme.default.regularFont(size: 22)
      label.textColor = Theme.default.normalTextColor
      return label
    }
    return nil
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
    case 0:
      guard let topNewDocument: Document = topNewDocument[safe: indexPath.row] else {
        return UITableViewCell()
      }
      let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableCell", for: indexPath) as! TaskTableCell
      cell.delegate = self
      cell.config(topNewDocument, false, indexPath.row)
      cell.btAttach.tag = indexPath.row
      cell.btAttach.addTarget(self, action: #selector(pressesBTAttach(_:)), for: .touchUpInside)
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "PieChartTableCell", for: indexPath) as! PieChartTableCell
      cell.config(self.chartCount)
      return cell
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "LineChartTableCell", for: indexPath) as! LineChartTableCell
      cell.setupChart(self.chartLine,self.viewType)
      cell.delegate = self
      return cell
    default:
      return UITableViewCell()
    }
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if (section == 0) {
      return 50
    }
    if (section == 2) {
      return 20
    }
    return 1
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    if section == 0 {
      let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
      view.backgroundColor = UIColor.white
      let line = UIView()
      line.backgroundColor = UIColor(hexString: "#D8D8D8")
      view.addSubview(line)
      line.snp.makeConstraints({ (make) in
        make.left.right.equalToSuperview()
        make.height.equalTo(0.5)
        make.centerY.equalToSuperview()
      })
      let button = UIButton(type: .custom)
      button.backgroundColor = UIColor.white
      if (isViewMore == false) {
        button.setTitle("Xem thêm", for: .normal)
      } else {
        button.setTitle("Ẩn lại", for: .normal)
      }
      button.setTitleColor(UIColor.darkGray, for: .normal)
      button.showsTouchWhenHighlighted = true
      button.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
      button.titleLabel?.font = UIFont(name: "Muli-Bold", size: 16.0)
      button.addTarget(self, action:#selector(self.pressesBTViewMore(_:)), for: .touchUpInside)
      view.addSubview(button)
      button.snp.makeConstraints({ (make) in
        make.width.equalTo(100)
        make.height.equalTo(40)
        make.center.equalToSuperview()
      })
      return view
    }
    return nil
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if (indexPath.section == 0) {
      let detailVC: TaskDetailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      detailVC.document = topNewDocument[indexPath.row]
        detailVC.didFinishDocument = { [weak self] in
            //self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            self?.needReload = true
        }
        detailVC.didupdatedDocument = { [weak self] in
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
      self.navigationController?.pushViewController(detailVC)
      //      topNewDocument[indexPath.row].unReadColor = "#777B9B"
      self.tableView.reloadRows(at: [indexPath], with: .automatic)
      InstanceDB.default.updateStatusDocument(documentID: topNewDocument[indexPath.row].id ?? "", done: { (result) in
      })
    }
  }
  
  func pressesBTAttach (_ sender: UIButton) {
    if (topNewDocument[sender.tag].fileDocuments != nil && topNewDocument[sender.tag].fileDocuments!.count == 1) {
      let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
      vc?.fileDoucment = topNewDocument[sender.tag].fileDocuments?[0]
      vc?.document = topNewDocument[sender.tag]
        vc?.delegate = self
      self.present(vc!, animated: true, completion: nil)
    } else {
      let vc: ListFileAttachViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
      vc.modalTransitionStyle = .crossDissolve
      vc.modalPresentationStyle =  .overCurrentContext
      vc.files = topNewDocument[sender.tag].fileDocuments ?? []
      vc.document = topNewDocument[sender.tag]
      self.navigationController?.present(vc, animated: true, completion: nil)
    }
  }
  
  func lineChartCellSelectViewType (viewType: ViewType) {
    self.viewType = viewType
    SVProgressHUD.show()
    InstanceDB.default.getChartLineOverviewHomeDashboard (viewType: self.viewType.rawValue) { (result) in
      SVProgressHUD.dismiss()
      self.chartLine.removeAll()
      self.chartLine.append(contentsOf: result)
      self.tableView.reloadData()
    }
  }
  
}

extension DashboardViewController: SwipeTableViewCellDelegate {  
  
  func tableView (_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
    if (indexPath.section == 0) {
      if orientation == .right {
        var list: [SwipeAction] = []
        if let document = self.topNewDocument[safe: indexPath.row],
          let actions = document.actions {
            let filterActions:[DocumentActionObject] = actions.filter {$0.actionType != .finish}.reversed()
          for i in 0..<filterActions.count {
            if (i == 3) {
              let action = SwipeAction(style: .default, title: "Xem thêm", handler: { (action, indexPath) in
                let actionSheet = UIAlertController(title: "Chọn", message: nil, preferredStyle: .actionSheet)
                for i in 0..<filterActions.count {
                  let action: UIAlertAction = UIAlertAction(title: filterActions[i].actionName, style: .default) { (_) in
                    if let actionType: DocumentAction = filterActions[i].actionType {
                      if actionType == .approve {
                        let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        assignVC.document = document
                        assignVC.indexItem = indexPath
                        assignVC.delegate = self
                        self.showViewController(viewController: assignVC, false)
                      } else if actionType == .assign {
                        let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        assignVC.delegate = self
                        assignVC.document = document
                        assignVC.getAssigners()
                        self.showViewController(viewController: assignVC, false)
                      } else if actionType == .process {
                        let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        processVC.delegate = self
                        processVC.document = document
                        self.showViewController(viewController: processVC, false)
                      } else if actionType == .appraise {
                        let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        appriseVC.delegate = self
                        appriseVC.document = document
                        appriseVC.indexPath = indexPath
                        self.showViewController(viewController: appriseVC, false)
                      } else if actionType == .sendMail {
                        let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                        sendEmailCV.document = document
                        sendEmailCV.delegate = self
                        self.showViewController(viewController: sendEmailCV, false)
                      } else if actionType == .finish {
                        self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
                      }
                    }
                  }
                  actionSheet.addAction(action)
                }
                let cancel: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { (_) in
                }
                actionSheet.addAction(cancel)
                self.present(actionSheet, animated: true, completion: nil)
              })
              action.image = #imageLiteral(resourceName: "multiSelect_more").filled(withColor: Theme.default.normalGreenSelectedColor)
              list.append(action)
              break
            } else {
              if let actionTypeItem: DocumentAction = filterActions[i].actionType {
                let action = SwipeAction(style: .default, title: filterActions[i].actionName, handler: { (action, indexPath) in
                })
                if actionTypeItem == .approve {
                  action.handler = { (action, indexPath) in
                    let assignVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                    assignVC.document = document
                    assignVC.indexItem = indexPath
                    assignVC.delegate = self
                    self.showViewController(viewController: assignVC, false)
                  }
                } else if actionTypeItem == .assign {
                  action.handler = { (action, indexPath) in
                    let assignVC: TaskAssignNoneApproveViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                    assignVC.delegate = self
                    assignVC.document = document
                    assignVC.getAssigners()
                    self.showViewController(viewController: assignVC, false)
                  }
                } else if actionTypeItem == .process {
                  action.handler = { (action, indexPath) in
                    let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                    processVC.delegate = self
                    processVC.document = document
                    self.showViewController(viewController: processVC, false)
                  }
                } else if actionTypeItem == .appraise {
                  action.handler = { (action, indexPath) in
                    let appriseVC: AppriseTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                    appriseVC.delegate = self
                    appriseVC.document = document
                    self.showViewController(viewController: appriseVC, false)
                  }
                } else if actionTypeItem == .sendMail {
                  action.handler = { (action, indexPath) in
                    let sendEmailCV: SendEmailViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                    sendEmailCV.document = document
                    sendEmailCV.delegate = self
                    self.showViewController(viewController: sendEmailCV, false)
                  }
                } else if actionTypeItem == .finish {
                  action.handler = { (action, indexPath) in
                    self.view.makeToast("Gửi yêu cầu kết thúc thành công.")
//                    let processVC: ProcessTaskViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
//                    processVC.delegate = self
//                    self.showViewController(viewController: processVC, false)
                  }
                }
                let imgAction: String = "ic_action_\(actionTypeItem.rawValue)"
                if let imgObj = UIImage(named: imgAction) {
                  action.image = imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                } else {
                  action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                }
                list.append(action)
              }
            }
          }
        }        
        for item in list {
          item.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
          item.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
          item.font = Theme.default.boldFont(size: 12)
        }
        return list
      } else if orientation == .left {
            if let document = self.topNewDocument[safe: indexPath.row],
                let actions = document.actions {
                let filterActions = actions.filter {$0.actionType == .finish}
                guard let docAction = filterActions.first else {
                    return []
                }
                let action = SwipeAction(style: .default, title: docAction.actionName, handler: { (action, index) in
                    self.finishDocument(document: document, indexPath: indexPath)
                })
                let imgAction: String = "ic_action_\(DocumentAction.finish.rawValue)"
                if let imgObj = UIImage(named: imgAction) {
                    action.image = imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                } else {
                    action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
                }
                action.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
                action.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                action.font = Theme.default.boldFont(size: 12)
                
                return [action]
            }
        }
    }
    return []
  }
  
  func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
    var options = SwipeTableOptions()
    options.expansionStyle = .selection
    options.transitionStyle = .drag
    options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    return options
  }    
  
}

extension DashboardViewController: TaskAssignNoneApproveDelegate {
  
  func onCloseView(status: Bool, indexPath: IndexPath?) {
    if status {
      self.view.makeToast("Xử lý thành công !")
        self.reloadItemHomeDashboard()
      if let index = indexPath {
        self.tableView.reloadRows(at: [index], with: UITableViewRowAnimation.automatic)
      }
    } else {
      self.view.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
    }
  }
  
}

extension DashboardViewController: ProcessTaskViewDelegate {
  
  func sendProcessWork(status: Bool, message: String?, index: IndexPath?) {
    if status {
      var msg = "Xử lý thành công !"
      if let msgTemp: String = message,
        msgTemp != "" {
        msg = msgTemp
      }
      self.view.makeToast(msg)
        self.reloadItemHomeDashboard()
      if let selectedIndex: IndexPath = index {
        self.tableView.reloadRows(at: [selectedIndex], with: UITableViewRowAnimation.automatic)
      }
    } else {
      self.view.makeToast("Có lỗi xảy ra, chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !")
    }
  }
  
  
  func closeView() {    
  }
  
}

extension DashboardViewController: AppriseTaskViewDelegate {
  
  func appriseCloseView() {
  }
  
  func appriseDoneView(status: Bool, message: String?, index: IndexPath?) {
    self.view.makeToast("Xử lý thành công !")
    self.reloadItemHomeDashboard()
    if let selectedIndex: IndexPath = index {
        self.tableView.reloadRows(at: [selectedIndex], with: UITableViewRowAnimation.automatic)
    }
  }
  
}

extension DashboardViewController: SendEmailViewDelegate {
  
  func sendEmailCloseView() {
    
  }
  
  func sendEmailDoneView() {
    self.view.makeToast("Gửi mail thành công !")
  }
  
}

extension DashboardViewController: TaskAssignNewViewDelegate {
  
  func taskAssignCloseView(_ index: IndexPath?, _ approveMsg: String?, _ returnMsg: String?, _ errorMsg: String?) {
    var defaultMsg: String = "Chúng tôi sẽ xử lý yêu cầu của bạn sớm nhất có thể !"
    if let confirmMsg: String = approveMsg {
      defaultMsg = confirmMsg
    } else if let backMsg: String = returnMsg {
      defaultMsg = backMsg
    } else if let failMsg: String = errorMsg {
      defaultMsg = failMsg
    }
    self.view.makeToast(defaultMsg)
    if let selectedIndex: IndexPath = index {
      self.tableView.reloadRows(at: [selectedIndex], with: UITableViewRowAnimation.automatic)
    }
  }
}

// MARK: - AttachmentDetailViewDelegate
extension DashboardViewController: AttachmentDetailViewDelegate {
    
    func doneView(rowIndex: IndexPath?, action: DocumentAction) {
        //self.view.makeToast(action.successMessage)
        if action != .sendMail, let selectedIndex = rowIndex {
            self.tableView.reloadRows(at: [selectedIndex], with: UITableViewRowAnimation.automatic)
        }
    }
}
