//
//  ForgetPassViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/6/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Alamofire

class ForgetPassViewController: UIViewController {

    @IBOutlet fileprivate weak var topLayout:NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var tfEmail:UITextField!
    @IBOutlet fileprivate weak var btNext:UIButton!
    @IBOutlet fileprivate weak var btCancel:UIButton!
    @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfEmail.text = "tmquan@lacviet.com.vn"
        registerNotification()
        self.checkBTNext()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfEmail.text = ""
        self.checkBTNext()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            
            if(UIDevice.is3InchesIPHONE() || UIDevice.is4InchesIPHONE()){
                self.topLayout.constant = -50
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            self.topLayout.constant = 30
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: nil, queue: nil) { (notification) in
            self.checkBTNext()
        }
    }
    
    func checkBTNext(){
        if tfEmail.text != ""{
            btNext.isEnabled = true
            btNext.backgroundColor = UIColor(red: 30, green: 106, blue: 194)
        }
        else{
            btNext.isEnabled = false
            btNext.backgroundColor = UIColor(red: 186, green: 186, blue: 186)
        }
    }
    
    
    
    
}
extension ForgetPassViewController{
    @IBAction func pressesBTNext(_ sender: UIButton){
        self.view.endEditing(true)
        self.loading.startAnimating()
        self.view.isUserInteractionEnabled = false
        self.sendCode()
    }
    @IBAction func pressesBTCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendCode(){
        LoginManager.sendCode(emailOrMobileNumber: tfEmail.text!) { (success, message) in
            if success{
                self.getCodeSuccess()
            }
            else{
                self.showAlert(message != "" ? message : "Không thể kết nối đến máy chủ!")
            }
        }
    }
    func getCodeSuccess(){
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
        let vc = self.storyboard?.instantiateViewController(withClass: ForgetPassVerifyViewController.self)
        vc?.email = tfEmail.text!
        self.present(vc!, animated: true, completion: nil)
    }
    
    func showAlert(_ message:String){
        loading.stopAnimating()
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "THÔNG BÁO", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Đồng Ý", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

