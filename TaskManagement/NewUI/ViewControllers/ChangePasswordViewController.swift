//
//  ChangePasswordViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 10/15/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet fileprivate weak var btExit:UIButton!
    @IBOutlet fileprivate weak var btConfirm:UIButton!
    
    @IBOutlet fileprivate weak var tfOldPassword:UITextField!
    @IBOutlet fileprivate weak var tfNewPassword:UITextField!
    @IBOutlet fileprivate weak var tfConfirmPassword:UITextField!
    
    @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkBTConfirm()
        registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: nil, queue: nil) { (notification) in
            self.checkBTConfirm()
        }
    }
    
    func checkBTConfirm(){
        if tfOldPassword.text != "" && tfNewPassword.text != "" && tfConfirmPassword.text != ""{
            btConfirm.isEnabled = true
            btConfirm.alpha = 1
        }
        else{
            btConfirm.isEnabled = false
            btConfirm.alpha = 0.7
        }
    }
}

extension ChangePasswordViewController{
    @IBAction func pressesExit(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func pressesConfirm(_ sender: UIButton){
        
    }
}
