//
//  TaskProcessViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/14/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class TaskProcessViewController: UIViewController {

    @IBOutlet fileprivate weak var bottonViewContent:NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle =  .overCurrentContext
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func pressesBTCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            UIView.animate(withDuration: 0.5, animations: {
                self.bottonViewContent.constant = 220
                self.view.layoutIfNeeded()
            })
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            UIView.animate(withDuration: 0.5, animations: {
                self.bottonViewContent.constant = 50
                self.view.layoutIfNeeded()
            })
        }
    }
}
