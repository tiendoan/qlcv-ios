//
//  AttachmentListViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/11/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SnapKit
import FSPagerView
import DropDown

class AttachmentListViewController: UIViewController{

    @IBOutlet fileprivate weak var pagerView:FSPagerView!
    @IBOutlet fileprivate weak var lbDetail:UILabel!
    
    fileprivate let mainColor = #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9294117647, alpha: 1)
    fileprivate let tintColor = #colorLiteral(red: 0.3882352941, green: 0.4039215686, blue: 0.537254902, alpha: 1)
    
    fileprivate var dropDown = DropDown()
    
    lazy fileprivate var btBack: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = UIColor.clear
        button.setImage(#imageLiteral(resourceName: "ic_arrow_back_white") , for: .normal)
        button.showsTouchWhenHighlighted = true
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        button.addTarget(self, action: #selector(actionBackButton), for: .touchUpInside)
        return button
    }()
    
    lazy fileprivate var btMore: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = UIColor.clear
        button.setImage(#imageLiteral(resourceName: "ic_more_vert_white") , for: .normal)
        button.showsTouchWhenHighlighted = true
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        button.addTarget(self, action: #selector(actionMoreButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = .init(rawValue: 0)
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationItem.title = "500HD-NH][05.06.2017].docx"
        
        self.navigationBar?.shadowImage = UIImage()
        self.navigationBar?.setColors(background: #colorLiteral(red: 0.1490196078, green: 0.4549019608, blue: 0.937254902, alpha: 1), text: .white)
        
        //let backBtn = UIBarButtonItem(customView: btBack)
        //self.navigationItem.leftBarButtonItem = backBtn
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btMore)
        
        self.setupPagerView()
        self.setupDropDown()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func actionBackButton(){
        self.navigationController?.popViewController()
    }
    func actionMoreButton(){
        dropDown.show()
    }
    
    func setupPagerView(){
        self.pagerView.transformer = FSPagerViewTransformer(type:.coverFlow)
        self.pagerView.isInfinite = false
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
        self.pagerView.itemSize = CGSize(width: UIScreen.main.bounds.width-100, height: UIScreen.main.bounds.height-200)
        lbDetail.text = "TẬP TIN 1/10"
    }
    func setupDropDown(){
        DropDown.startListeningToKeyboard()
        
        //More action view config
        DropDown.appearance().textColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
        DropDown.appearance().textFont = UIFont(name: "Muli-Regular", size: 19)!
        dropDown.anchorView = self.navigationItem.rightBarButtonItem
        dropDown.dataSource = ["Thao tác", "Giao việc", "Chuyển tiếp", "Chỉnh sửa", "Khác"]
        let icon = [#imageLiteral(resourceName: "ic_more_thaotac"), #imageLiteral(resourceName: "ic_more_giaoviec"), #imageLiteral(resourceName: "ic_more_chuyentiep"), #imageLiteral(resourceName: "ic_more_edit"), #imageLiteral(resourceName: "ic_more_orther")]
        dropDown.cellNib = UINib(nibName: "ActionPopupDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? ActionPopupDropDownCell else { return }
            // Setup your custom UI components
            cell.iconImageView.image = icon[index]
        }
        dropDown.width = self.view.widthf - 20
        dropDown.bottomOffset = CGPoint(x: 50, y: 50)
        //[unowned self]
        dropDown.selectionAction = {  (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            switch index {
            case 1:
                let assginVC: TaskAssignNewViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                self.showViewController(viewController: assginVC)
            case 2:
                let forwardVC: TaskForwardViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                self.showViewController(viewController: forwardVC)
            case 3:
                let assginVC: TaskEditViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                self.showViewController(viewController: assginVC)
            case 4:
                let forwardVC: TaskProcessViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
                self.showViewController(viewController: forwardVC)
            default:
                break
            }
        }
    }
       
}
extension AttachmentListViewController: FSPagerViewDataSource, FSPagerViewDelegate{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 10
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)

        cell.backgroundColor = UIColor.white
        cell.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 10)
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 5.0
        
        cell.imageView?.image = #imageLiteral(resourceName: "img_test")
        
        
        return cell
    }
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        lbDetail.text = "TẬP TIN \(self.pagerView.currentIndex+1)/10"
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        let vc = self.storyboard?.instantiateViewController(withClass: AttachmentDetailViewController.self)
        //self.navigationController?.pushViewController(vc!)
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
}
