//
//  NewMainViewController.swift
//  TaskManagement
//
//  Created by HaiComet on 9/4/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Pageboy
import Tabman
import SVProgressHUD
import KYDrawerController

class NewMainViewController: TabmanViewController {
  
  lazy fileprivate var btMenu: UIButton = {
    let button = UIButton(type: .custom)
    button.backgroundColor = UIColor.clear
    button.setImage(#imageLiteral(resourceName: "ic_menu").filled(withColor: UIColor.white), for: .normal)
    button.showsTouchWhenHighlighted = true
    button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
    button.addTarget(self, action: #selector(actionMenuButton), for: .touchUpInside)
    return button
  }()
  
  lazy fileprivate var btSearch: UIButton = {
    let button = UIButton(type: .custom)
    button.tintColor = Theme.default.normalWhiteColor
    button.setImage(#imageLiteral(resourceName: "ic_search").filled(withColor: UIColor.white), for: .normal)
    button.showsTouchWhenHighlighted = true
    button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
    button.addTarget(self, action: #selector(actionSearchButton(_:)), for: .touchUpInside)
    return button
  }()
  
  lazy fileprivate var btNotification: SSBadgeButton = {
    let button = SSBadgeButton(type: .custom)
    button.tintColor = Theme.default.normalWhiteColor
    button.setImage(#imageLiteral(resourceName: "ic_newnotification").filled(withColor: UIColor.white), for: .normal)
    button.showsTouchWhenHighlighted = true
    button.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
    button.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 5)
    button.addTarget(self, action: #selector(actionNotificationButton), for: .touchUpInside)
    return button
  }()
  
  lazy fileprivate var btTitle: UIButton = {
    let button = UIButton(type: .custom)
    button.backgroundColor = UIColor.clear
    button.setTitle("TRANG CHỦ  ▼", for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.frame = CGRect(x: 0, y: 0, width: 225, height: 40)
    button.titleLabel?.font = UIFont(name: "Muli-Bold", size: 18)!
    button.titleLabel?.minimumScaleFactor = 0.7
    button.titleLabel?.adjustsFontSizeToFitWidth = true
    button.showsTouchWhenHighlighted = true
    button.sizeToFit()
    button.addTarget(self, action: #selector(actionTitle), for: .touchUpInside)
    return button
  }()
  
  @IBOutlet fileprivate weak var tabbleView: UITableView!
  fileprivate var indexRootMenu = 0
  @IBOutlet fileprivate weak var topMenu:NSLayoutConstraint!
  @IBOutlet fileprivate weak var heightMenu:NSLayoutConstraint!
  @IBOutlet fileprivate weak var viewMenu:UIView!
  var isShowMenu = false
  
  fileprivate var rootMenus:[Menu] = []
  fileprivate var childMenus:[Menu] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.edgesForExtendedLayout = .init(rawValue: 0)
    
    self.navigationBar?.shadowImage = UIImage()
    self.navigationBar?.setColors(background: #colorLiteral(red: 0.1490196078, green: 0.4549019608, blue: 0.937254902, alpha: 1), text: UIColor.white)
    
    self.dataSource = self
    self.isScrollEnabled = false
    
    let menuBtn = UIBarButtonItem(customView: btMenu)
    self.navigationItem.leftBarButtonItem = menuBtn
    
    let searchBtn = UIBarButtonItem(customView: btSearch)
    let notificationBtn = UIBarButtonItem(customView: btNotification)
    self.navigationItem.rightBarButtonItems = [searchBtn,notificationBtn];
    
    self.navigationItem.titleView = btTitle
    
    tabbleView.dataSource = self
    tabbleView.delegate = self
    tabbleView.register(UITableViewCell.self, forCellReuseIdentifier: "CELL")
    
    viewMenu.backgroundColor = UIColor(white: 0, alpha: 0)
    topMenu.constant = -200
    self.view.layoutIfNeeded()
    self.viewMenu.isUserInteractionEnabled = false
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(exitMenu))
    viewMenu.addGestureRecognizer(tap)
    
    self.getRootMenu()
    
    DataManager.shared.getAllUsersInfo()
    DataManager.shared.getAllEmployees()
    registerNotification()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  override var preferredStatusBarStyle: UIStatusBarStyle{
    return .lightContent
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if UIApplication.shared.applicationIconBadgeNumber > 0 {
        btNotification.badgeLabel.isHidden = false
        btNotification.badge = "\(UIApplication.shared.applicationIconBadgeNumber)"
    } else {
        btNotification.badgeLabel.isHidden = true
    }
    
    //self.navigationBar?.setColors(background: #colorLiteral(red: 0.862745098, green: 0.8823529412, blue: 0.9294117647, alpha: 1), text: #colorLiteral(red: 0.3882352941, green: 0.4039215686, blue: 0.537254902, alpha: 1))
  }
  
  
  func actionMenuButton(){
    self.mainNew()?.toggleMenu()
  }
  func actionTitle(){
    self.showMenu(show: !isShowMenu)
  }
  
  @IBAction func actionSearchButton(_ sender: UIButton ) {
    let searchVC: SearchViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    self.showViewController(viewController: searchVC, false)
  }
  
  func actionNotificationButton(){
    let notificationsVC: NotificationsViewController = UIStoryboard(storyboard: .newMain).instantiateViewController()
    self.showViewController(viewController: notificationsVC, false)
  }
  func exitMenu(){
    self.showMenu(show: !isShowMenu)
  }
  
  func getRootMenu(){
    SVProgressHUD.show()
    InstanceDB.default.getMenu() { (result) in
      SVProgressHUD.dismiss()
      self.rootMenus.removeAll()
      self.rootMenus.append(contentsOf: result)
      self.tabbleView.reloadData()
      
      var height = (self.rootMenus.count+1)*44
      height = max(height, 200)
      height = min(height, 400)
      self.heightMenu.constant = CGFloat(height)
      self.topMenu.constant = -self.heightMenu.constant
      self.view.layoutIfNeeded()
      
      if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
        leftVC.loadRootMenu(self.rootMenus)
      }
      
      if(self.rootMenus.count > 0){
        self.getMenuChildNodes(0)
      }
    }
  }
    func getMenuChildNodes(_ index: Int, _ isLoadBoard: Bool = false, updateMenuOnly:Bool = false){
    SVProgressHUD.show()
    if let selectedMenu: Menu = self.rootMenus[safe: index],
      let selectedId: String = selectedMenu.id {
      InstanceDB.default.getMenu(parentId: selectedId, treeFilterCode: self.rootMenus[index].code ?? "") { (result) in
        SVProgressHUD.dismiss()
        self.childMenus.removeAll()
        self.childMenus.append(contentsOf: result)
        if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
          leftVC.loadMenuChildNotes(self.rootMenus[index].name ?? "",self.childMenus)
          leftVC.selectRootMenu(index)
        }
        if isLoadBoard{
          if let vc = self.viewControllers![1] as? BoardViewController{
            vc.setupMenus(self.childMenus, 0, updateMenuOnly: updateMenuOnly)
          }
        }
      }
    }
  }
  
  func registerNotification(){
    NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_BADGE_UPDATED), object: nil, queue: nil) { (notification) in
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            self.btNotification.badgeLabel.isHidden = false
            self.btNotification.badge = "\(UIApplication.shared.applicationIconBadgeNumber)"
        } else {
            self.btNotification.badgeLabel.isHidden = true
        }
    }
    NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_ROOT), object: nil, queue: nil) { (notification) in
      if let item = notification.object as? Int{
        self.indexRootMenu = item + 1
        self.tabbleView.reloadData()
        self.showMenu(show: false)
        
        self.selectRootMenuAtIndex(index: item)
      }
    }
    NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_PARENT), object: nil, queue: nil) { (notification) in
      if let dict = notification.object as? [String:Int]{
        let parent = dict["IndexParent"]!
        let child = dict["IndexChild"]!
        
        self.indexRootMenu = parent+1
        self.tabbleView.reloadData()
        self.btTitle.setTitle((self.rootMenus[parent].name ?? "") + "  ▼", for: .normal)
        self.btTitle.sizeToFit()
        self.scrollToPage(PageboyViewController.PageIndex.at(index: 1), animated: false)
        if let vc = self.viewControllers![1] as? BoardViewController{
            vc.setupMenus(self.childMenus, child)
        }
      }
    }
    NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_MENU_SELECT_CHILD), object: nil, queue: nil) { (notification) in
      if let dict = notification.object as? [String:Int]{
        let parent = dict["IndexParent"]
        let child = dict["IndexChild"]
        
        self.scrollToPage(PageboyViewController.PageIndex.at(index: 1), animated: false)
        if let vc = self.viewControllers![1] as? BoardViewController{
          vc.setupMenus(self.childMenus[parent!].childrens!, child!)
        }
      }
    }
    
    // Finish documents -> update badge number
    NotificationCenter.default.addObserver(forName: NSNotification.Name.init(NotificationName.POST_NOTIFICAITON_FINISHED_DOCUMENT), object: nil, queue: nil) { (notification) in
        if self.indexRootMenu == 0 || self.indexRootMenu == 1 || self.indexRootMenu == 4 {
            let index = self.indexRootMenu == 4 ? 3 : 0
            if(self.rootMenus.count > 0){
                self.getMenuChildNodes(index, true, updateMenuOnly: true)
            }
        }
    }
  }
}

extension NewMainViewController: UITableViewDataSource, UITableViewDelegate{
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return rootMenus.count+1
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tabbleView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
    cell.backgroundColor = UIColor.clear
    cell.selectionStyle = .none
    cell.textLabel?.font = UIFont(name: "Muli-Bold", size: 15)!
    cell.textLabel?.textAlignment = .center
    cell.textLabel?.textColor = UIColor.white//UIColor.init(red: 99, green: 103, blue: 137)
    if(indexPath.row == 0){
      cell.textLabel?.text = "TRANG CHỦ"
    }
    else{
      cell.textLabel?.text = rootMenus[indexPath.row-1].name
    }
    if(indexPath.row == self.indexRootMenu){
      cell.textLabel?.alpha = 1
    }
    else{
      cell.textLabel?.alpha = 0.5
    }
    return cell
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.indexRootMenu = indexPath.row
    tableView.reloadData()
    self.showMenu(show: false)
    if(self.indexRootMenu == 0){
      self.btTitle.setTitle("TRANG CHỦ  ▼", for: .normal)
      self.scrollToPage(PageboyViewController.PageIndex.at(index: 0), animated: false)
      self.getMenuChildNodes(0)
      self.btTitle.sizeToFit()
      if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
        leftVC.selectRootMenu(-1)
      }
    }
    else{
      self.selectRootMenuAtIndex(index: self.indexRootMenu - 1)
    }
    
    
  }
  
  fileprivate func selectRootMenuAtIndex(index: Int){
    self.btTitle.setTitle((rootMenus[index].name ?? "") + "  ▼", for: .normal)
    self.scrollToPage(PageboyViewController.PageIndex.at(index: 1), animated: false)
    self.getMenuChildNodes(index, true)
    self.btTitle.sizeToFit()
    if let leftVC = self.mainNew()?.drawerViewController as? LeftViewController{
      leftVC.selectRootMenu(index)
    }
  }
}


extension NewMainViewController:PageboyViewControllerDataSource{
  func viewControllers(forPageboyViewController pageboyViewController: PageboyViewController) -> [UIViewController]? {
    
    var viewControllers:[Any] = []
    
    let dashboardVC = self.storyboard?.instantiateViewController(withClass: DashboardViewController.self)
    let boardVC = self.storyboard?.instantiateViewController(withClass: BoardViewController.self)
    viewControllers.append(dashboardVC!)
    viewControllers.append(boardVC!)
    return viewControllers as? [UIViewController]
    
  }
  
  func defaultPageIndex(forPageboyViewController pageboyViewController: PageboyViewController) -> PageboyViewController.PageIndex? {
    return nil
  }
}

extension NewMainViewController{
  fileprivate func showMenu(show: Bool){
    if(show){
      UIView.animate(withDuration: 0.3, animations: {
        self.viewMenu.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.topMenu.constant = 0
        self.view.layoutIfNeeded()
      }, completion: { (success) in
        self.viewMenu.isUserInteractionEnabled = true
        self.isShowMenu = true
      })
    }
    else{
      UIView.animate(withDuration: 0.3, animations: {
        self.viewMenu.backgroundColor = UIColor(white: 0, alpha: 0)
        self.topMenu.constant = -self.heightMenu.constant
        self.view.layoutIfNeeded()
      }, completion: { (success) in
        self.viewMenu.isUserInteractionEnabled = false
        self.isShowMenu = false
      })
    }
  }
  
  
}
