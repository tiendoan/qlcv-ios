//
//  TaskForwardViewController.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/14/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class TaskForwardViewController: UIViewController {

    @IBOutlet fileprivate weak var tableView:UITableView!
    @IBOutlet fileprivate weak var btCancel:UIButton!
    @IBOutlet fileprivate weak var btDone:UIButton!
    @IBOutlet fileprivate weak var tfNote:UITextField!
    
    @IBOutlet fileprivate weak var bottonViewContent:NSLayoutConstraint!
    
    fileprivate let arrayParent = ["Ban Giám Đốc", "Trụ sở chính", "Trụ sở Hà Nội", "Bộ phận Văn Thư"]
    fileprivate let arrayChild = ["Phòng CNTT","Phòng Hành Chánh","Phòng Nhân Sự","Phòng Kết Hoạch"]
    
    fileprivate var selectIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.automaticallyAdjustsScrollViewInsets = false
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle =  .overCurrentContext
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        registerNotification()
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.register(withClass: TaskForwardTableCell.self)
        //tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = 47
        tableView.dataSource = self
        tableView.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func pressesBTCancel(_ sender: UIButton){
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func pressesBTDone(_ sender: UIButton){
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { (notification) in
            UIView.animate(withDuration: 0.5, animations: {
                self.bottonViewContent.constant = 220
                self.view.layoutIfNeeded()
            })
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { (notification) in
            UIView.animate(withDuration: 0.5, animations: {
                self.bottonViewContent.constant = 50
                self.view.layoutIfNeeded()
            })
        }
    }
}
extension TaskForwardViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayParent.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == selectIndex){
            return 46.5 + CGFloat(47*arrayChild.count) - 16
        }
        return 46.5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskForwardTableCell", for: indexPath) as! TaskForwardTableCell
        cell.btShow.tag = indexPath.row
        cell.btShow.addTarget(self, action: #selector(pressesBTShow(_:)), for: .touchUpInside)
        
        var show = false
        if(indexPath.row == selectIndex){
            show = true
        }
        
        if indexPath.row == 0{
            cell.config(arrayParent[indexPath.row], .Top, nil, false)
            cell.btShow.setTitle("", for: .normal)
        }
        else if indexPath.row == arrayParent.count - 1{
            cell.config(arrayParent[indexPath.row], .Botton, arrayChild, show)
            cell.btShow.setTitle("+", for: .normal)
        }
        else{
            cell.config(arrayParent[indexPath.row], .Middle, arrayChild, show)
            cell.btShow.setTitle("+", for: .normal)
        }
        
        return cell
    }
    
    func pressesBTShow(_ sender: UIButton){
        if(sender.tag > 0){
            
            if(selectIndex == sender.tag){
                selectIndex = -1
            }
            else{
                selectIndex = sender.tag
            }
            tableView.beginUpdates()
            //tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
            tableView.endUpdates()
        }
    }
}
