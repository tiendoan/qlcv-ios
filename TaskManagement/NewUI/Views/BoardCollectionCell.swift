//
//  BoardCollectionCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwipeCellKit
import DropDown

protocol BoardCollectionCellDelegate {
    func taskCellDidSelected(document: Document?, itemIndex:IndexPath?, rowIndex: IndexPath?)
  func taskCellAttachSelected(document: Document?, rowIndex: IndexPath?)
  func actionHistoryProcess(document: Document?)
  func assignTaskProcess(document: Document?, index: IndexPath?)
  func approveTaskProcess(document: Document?, index: IndexPath?)
  func processTaskProcess(document: Document?, index: IndexPath?)
  func appriseTaskProcess(document: Document?, index: IndexPath?)
  func emailTaskProcess(document: Document?, index: IndexPath?)
  func showAllProcess(document: Document?, index: IndexPath?)
  func finishTaskProcess(document: Document?, index: IndexPath?)
}

class BoardCollectionCell: UICollectionViewCell, UITableViewDataSource, UITableViewDelegate {
  
    @IBOutlet weak var vwContent: UIView!
  @IBOutlet fileprivate weak var tableview:UITableView!
  @IBOutlet fileprivate weak var loading:UIActivityIndicatorView!
  @IBOutlet fileprivate weak var btItemCheck:UIButton!
  @IBOutlet fileprivate weak var viewMultiSelect:UIView!
    @IBOutlet fileprivate weak var customView:UIView!
    
  var delegate: BoardCollectionCellDelegate?
  var indexItem: IndexPath?
  
  fileprivate var menu:Menu!
  fileprivate var documents:[Document] = []
    fileprivate var documentData:DocumentData?
  fileprivate var page = 1
  fileprivate var isLoadMore = false
  fileprivate var canLoadMore = false
    
  fileprivate var allSelect:[Int] = []
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    tableview.separatorStyle = .none
    tableview.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
    tableview.dataSource = self
    tableview.delegate = self
    tableview.rowHeight = UITableViewAutomaticDimension
    tableview.estimatedRowHeight = UITableViewAutomaticDimension
    tableview.register(withClass: TaskTableCell.self)
    let refresh = UIRefreshControl()
    refresh.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    tableview.addSubview(refresh)
    let longPressGesture = UILongPressGestureRecognizer(target: self, action: (#selector(handleLongPress(_:))))
    self.tableview.addGestureRecognizer(longPressGesture)
    self.vwContent.layer.masksToBounds = true
  }
    
    func subMenuTapFunction(sender:UITapGestureRecognizer) {
        
        // Present dialog
        let dropDown = DropDown()
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        
        dropDown.show()
        
    }
    
    private func createPopup() {
        
        let dropDown = DropDown()
        

        // The view to which the drop down will appear on
        dropDown.anchorView = customView // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        
        dropDown.show()
    }
  
  func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
    let point = gestureRecognizer.location(in: self.tableview)
    let indexPath = self.tableview.indexPathForRow(at: point)
    if indexPath != nil {
      if gestureRecognizer.state == .began {
        selectItemAtIndex(indexPath!.row)
        //tableview.reloadRows(at: [indexPath!], with: .automatic)
        let cell = tableview.cellForRow(at: indexPath!) as! TaskTableCell
        cell.viewCheck.isHidden = !checkSelectAtIndex(indexPath!.row)
      }
    }
  }
  
  func selectItemAtIndex(_ index:Int) {
    var isYes = false
    for i in 0..<allSelect.count {
      if allSelect[i] == index {
        isYes = true
        allSelect.remove(at: i)
        break
      }
    }
    if isYes == false {
      allSelect.append(index)
    }
    
    if allSelect.count > 0 {
      btItemCheck.isHidden = false
      viewMultiSelect.isHidden = false
    } else {
      btItemCheck.isHidden = true
      viewMultiSelect.isHidden = true
    }
  }
  
  func checkSelectAtIndex(_ index: Int) -> Bool {
    var isCheck = false
    if self.allSelect.isEmpty {
      return isCheck
    }
    for i in 0..<allSelect.count {
      if allSelect[i] == index{
        isCheck = true
        break
      }
    }
    return isCheck
  }
  
    func config(_ menu: Menu) {
    self.menu = menu
    let str = self.menu?.name?.uppercased() ?? ""
    //self.lbTitle.text = "  \(str)  ▼  "
    self.allSelect.removeAll()
    //self.lbTitle.addTarget(self, action: #selector(actionSubTitle), for: .touchUpInside);
    btItemCheck.isHidden = true
    viewMultiSelect.isHidden = true
    getDocument()
        
        
  }

  
  func getDocument() {
    self.page = 1
    self.tableview.isHidden = true
    self.tableview.contentOffset = CGPoint(x: 0, y: -15)
    self.loading.startAnimating()
    InstanceDB.default.getDocument(treefilterID: menu.id ?? "", page: page) { (result) in
      self.loading.stopAnimating()
      self.tableview.isHidden = false
      self.documentData = result
      self.documents.removeAll()
      self.documents.append(contentsOf: self.documentData?.items ?? [])
      self.tableview.reloadData()
        
        self.tableview.tableFooterView = nil
        self.tableview.tableFooterView?.isHidden = true
        
    }
  }
  
  func refresh(_ sender:UIRefreshControl?) {
    self.page = 1
    InstanceDB.default.getDocument(isPull: true,treefilterID: menu.id ?? "", page: page) { (result) in
      sender?.endRefreshing()
        SVProgressHUD.dismiss()
      self.documents.removeAll()
      if(result != nil){
        self.documentData = result
        self.documents.append(contentsOf: self.documentData?.items ?? [])
        self.tableview.reloadData()
        self.tableview.tableFooterView = nil
        self.tableview.tableFooterView?.isHidden = true
      }
    }
  }
  
  func loadMoreData() {
    if(isLoadMore == false && documentData != nil && documentData!.page != nil && documentData!.page!.totalPages != nil && documentData!.page!.totalPages! > page){
        canLoadMore = true
      self.page = self.page + 1
      InstanceDB.default.getDocument(treefilterID: menu.id ?? "", page: page) { (result) in
        if (result != nil) {
          self.documentData = result
          self.documents.insert(contentsOf: result!.items ?? [], at: self.documents.count)
          self.tableview.reloadData()
            self.tableview.tableFooterView = nil
            self.tableview.tableFooterView?.isHidden = true
        }
      }
    } else {
        canLoadMore = false
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return documents.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let documentItem: Document = documents[safe: indexPath.row] else {
      return UITableViewCell()
    }
    let cell = tableView.dequeueReusableCell(withIdentifier: "TaskTableCell", for: indexPath) as! TaskTableCell
    cell.delegate = self
    cell.config(documentItem, checkSelectAtIndex(indexPath.row), indexPath.row)
    cell.btAttach.tag = indexPath.row
    cell.btAttach.addTarget(self, action: #selector(pressesBTAttach(_:)), for: .touchUpInside)
    return cell
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if (indexPath.row == documents.count - 2) {
      loadMoreData()
    }

    let lastRowIndex = documents.count - 1
    if indexPath.row == lastRowIndex && canLoadMore {
        // print("this is the last cell")
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        
        self.tableview.tableFooterView = spinner
        self.tableview.tableFooterView?.isHidden = false
    } else {
        self.tableview.tableFooterView = nil
        self.tableview.tableFooterView?.isHidden = true
    }
  }

    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if (allSelect.count == 0) {
      self.documents[indexPath.row].unReadColor = "#777B9B"
      self.tableview.reloadRows(at: [indexPath], with: .automatic)
        self.delegate?.taskCellDidSelected(document: documents[indexPath.row], itemIndex: indexItem, rowIndex: indexPath)
    } else {
      selectItemAtIndex(indexPath.row)
      //tableview.reloadRows(at: [indexPath!], with: .automatic)
      let cell = tableview.cellForRow(at: indexPath) as! TaskTableCell
      cell.viewCheck.isHidden = !checkSelectAtIndex(indexPath.row)
    }
  }
  
  @objc func pressesBTAttach(_ sender: UIButton){
    if allSelect.count == 0 {
      self.delegate?.taskCellAttachSelected(document: documents[sender.tag], rowIndex: nil)
    }
  }
    
    func finishDocument(document: Document, indexPath:IndexPath) {
        SVProgressHUD.show()
        DataManager.finishDocument(document: document, done: { (finish) in
            if finish {
                self.makeToast("Gửi yêu cầu kết thúc thành công.")
                if let menuCode = self.menu.code, menuCode == MenuCode.new.rawValue || menuCode == MenuCode.processing.rawValue {
                    SVProgressHUD.dismiss()
                    self.documents.remove(at: indexPath.row)
                    self.tableview.beginUpdates()
                    self.tableview.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                    self.tableview.endUpdates()
                } else {
                    self.refresh(nil)
                }
                
            } else {
                SVProgressHUD.dismiss()
                self.makeToast("Có lỗi xảy ra, hệ thống sẽ cập nhật cho bạn sớm nhất !")
            }
        })
    }
}

extension BoardCollectionCell: SwipeTableViewCellDelegate {
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
    guard orientation == .right else {
        if let document = self.documents[safe: indexPath.row],
            let actions = document.actions {
            let filterActions:[DocumentActionObject] = actions.filter {$0.actionType == .finish}
            guard let docAction = filterActions.first else {
                return []
            }
            let action = SwipeAction(style: .default, title: docAction.actionName, handler: { (action, index) in
                self.finishDocument(document: document, indexPath: indexPath)
            })
            let imgAction: String = "ic_action_\(DocumentAction.finish.rawValue)"
            if let imgObj = UIImage(named: imgAction) {
                action.image = imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            } else {
                action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
            }
            action.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
            action.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            action.font = Theme.default.boldFont(size: 12)
            
            return [action]
        }
      return nil
    }
    if (indexPath.section == 0) {
      var list: [SwipeAction] = []
      if let document = self.documents[safe: indexPath.row],
        let actions = document.actions {
        let filterActions:[DocumentActionObject] = actions.filter {$0.actionType != .finish}.reversed()
        for i in 0..<filterActions.count {
          if (i == 3) {
            let action = SwipeAction(style: .default, title: "Xem thêm", handler: { (action, indexPath) in
              debugPrint("Defaule Board Collection Cell More: \(action.debugDescription) - indexPath \(indexPath.debugDescription)")
              self.delegate?.showAllProcess(document: document, index: self.indexItem)
            })
            action.image = #imageLiteral(resourceName: "multiSelect_more").filled(withColor: Theme.default.normalGreenSelectedColor)
            list.append(action)
            break
          } else {
            if let actionTypeItem: DocumentAction = filterActions[i].actionType {
              let action = SwipeAction(style: .default, title: filterActions[i].actionName, handler: { (action, indexPath) in
                debugPrint("Default Board Collection Cell: \(action.debugDescription) - indexPath \(indexPath.row)")
              })
              if actionTypeItem == .assign {
                action.handler = { (action, indexPath) in
                  self.delegate?.assignTaskProcess(document: document, index: self.indexItem)
                }
              } else if actionTypeItem == .approve {
                action.handler = { (action, indexPath) in
                  self.delegate?.approveTaskProcess(document: document, index: self.indexItem)
                }
              } else if actionTypeItem == .process {
                action.handler = { (action, indexPath) in
                  self.delegate?.processTaskProcess(document: document, index: self.indexItem)
                }
              } else if actionTypeItem == .appraise {
                action.handler = { (action, indexPath) in
                  self.delegate?.appriseTaskProcess(document: document, index: self.indexItem)
                }
              } else if actionTypeItem == .sendMail {
                action.handler = { (action, indexPath) in
                  self.delegate?.emailTaskProcess(document: document, index: self.indexItem)
                }
              } else if actionTypeItem == .finish {
                action.handler = { (action, indexPath) in
                  self.delegate?.finishTaskProcess(document: document, index: self.indexItem)
                }
              }
              let imgAction: String = "ic_action_\(actionTypeItem.rawValue)"
              if let imgObj = UIImage(named: imgAction) {
                action.image = imgObj.imageResize(sizeChange: CGSize(width: 40, height: 40)).filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
              } else {
                action.image =  #imageLiteral(resourceName: "multiSelect_more").filled(withColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
              }
              list.append(action)
            }
          }
        }
      }      
      for item in list {        
        item.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.9058823529, blue: 0.9411764706, alpha: 1)
        item.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        item.font = Theme.default.boldFont(size: 12)
      }
      return list
    } else {
        
    }
    return []
  }
  
  func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
    var options = SwipeTableOptions()
    options.expansionStyle = .selection
    options.transitionStyle = .drag
    options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    return options
  }
}
