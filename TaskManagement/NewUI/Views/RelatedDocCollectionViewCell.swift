//
//  RelatedDocCollectionViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 10/16/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class RelatedDocCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
//      layoutIfNeeded()
      bgView.cornerRadius = 15
      bgView.clipsToBounds = true
        // Initialization code
    }

}
