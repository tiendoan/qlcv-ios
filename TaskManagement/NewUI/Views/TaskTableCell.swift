//
//  TaskTableCell.swift
//  TaskManagement
//
//  Created by HaiComet on 9/7/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import BubblePictures
import Kingfisher
import SwipeCellKit
import Pastel

class TaskTableCell: SwipeTableViewCell, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
  
  @IBOutlet fileprivate weak var lineKhan:UIView!
  @IBOutlet fileprivate weak var ic_Khan1:UIImageView!
  @IBOutlet fileprivate weak var btKhan1:UIButton!
  @IBOutlet fileprivate weak var ic_Khan2:UIImageView!
  @IBOutlet fileprivate weak var btKhan2:UIButton!
  @IBOutlet fileprivate weak var widthKhan1:NSLayoutConstraint!
  @IBOutlet fileprivate weak var widthKhan2:NSLayoutConstraint!
  
  @IBOutlet fileprivate weak var viewInfo:UIView!
  @IBOutlet fileprivate weak var imgAvatar:UIImageView!
  @IBOutlet fileprivate weak var lbName:UILabel!
  @IBOutlet fileprivate weak var lbPosition:UILabel!
  @IBOutlet fileprivate weak var heightUserInfo:NSLayoutConstraint!
  
  @IBOutlet fileprivate weak var lbContent:UILabel!
  
  @IBOutlet fileprivate weak var imgTitle:UIImageView!
  @IBOutlet fileprivate weak var lbTitle:UILabel!
  @IBOutlet fileprivate weak var heightTitle:NSLayoutConstraint!
  @IBOutlet fileprivate weak var widthImgTitle:NSLayoutConstraint!
  
  @IBOutlet fileprivate weak var lbTitleCode:UILabel!
  @IBOutlet fileprivate weak var lbCode:UILabel!
  
  @IBOutlet fileprivate weak var lbTitleDate:UILabel!
  @IBOutlet fileprivate weak var lbDate:UILabel!
  
  @IBOutlet fileprivate weak var lbTag:UILabel!
  @IBOutlet fileprivate weak var btComment:UIButton!
  @IBOutlet weak var btAttach:UIButton!
  
  @IBOutlet fileprivate weak var heightBTAttach:NSLayoutConstraint!
  @IBOutlet weak var collectionView: UICollectionView!
  private var bubblePictures: BubblePictures!
  fileprivate var trackingAvatars:[String] = []
  
  @IBOutlet weak var viewCheck:UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.white
    self.selectionStyle = .none
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CELL")
    ic_Khan1.image = #imageLiteral(resourceName: "ic_mat").filled(withColor: UIColor(red: 210, green: 0, blue: 21))
    btKhan1.backgroundColor = UIColor(red: 210, green: 0, blue: 21)
    ic_Khan2.image = #imageLiteral(resourceName: "ic_mat").filled(withColor: UIColor(red: 210, green: 0, blue: 21))
    btKhan2.backgroundColor = UIColor(red: 210, green: 0, blue: 21)
    
    btAttach.setTitleColor(UIColor.white, for: .normal)
    
  }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
    func config(_ document: Document, _ select: Bool = false, _ index: Int, searchText: String = "" ) {
    trackingAvatars.removeAll()
    viewCheck.isHidden = !select
    let textPriority = document.priorityDocument?.name ?? ""
    let textSecret = document.secretDocument?.name ?? ""
    if (index == 0) {
      lineKhan.backgroundColor = UIColor.clear
    } else {
      lineKhan.backgroundColor = UIColor(hexString: "#D8D8D8")
    }
    if (textPriority != "" && textSecret != "") {
      let str = "  " + textPriority + " - " + textSecret + "   "
      btKhan1.setTitle(str, for: .normal)
      btKhan2.setTitle(str, for: .normal)
      let size = NSString(string: str).boundingRect(with: CGSize(width: Double.greatestFiniteMagnitude, height: 20),options: NSStringDrawingOptions.usesLineFragmentOrigin,attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 10)], context: nil).size
      widthKhan1.constant = size.width + 40
      widthKhan2.constant = size.width + 40
    } else if (textPriority == "" && textSecret == "") {
      btKhan1.setTitle("", for: .normal)
      btKhan2.setTitle("", for: .normal)
      widthKhan1.constant = 0
      widthKhan2.constant = 0
    } else {
      let str = "  " + (textPriority != "" ? textPriority : textSecret) + "   "
      btKhan1.setTitle(str, for: .normal)
      btKhan2.setTitle(str, for: .normal)
      let size = NSString(string: str).boundingRect(with: CGSize(width: Double.greatestFiniteMagnitude, height: 20),options: NSStringDrawingOptions.usesLineFragmentOrigin,attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 10)], context: nil).size
      widthKhan1.constant = size.width + 40
      widthKhan2.constant = size.width + 40
    }
    if(document.type != nil && document.type! == .task) {
      widthKhan2.constant = 0
      lbName.text = document.approvedByName
      lbPosition.text = document.approvedByJobTitle
      let url = URL(string: Constants.default.domainAddress + (document.approvedByPicture ?? ""))
      if url != nil {
        self.imgAvatar.kf.setImage(with: url!, placeholder: UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50)), options: nil, progressBlock: nil, completionHandler: nil)
      } else {
        self.imgAvatar.image = UIImage(color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
      }
      heightUserInfo.constant = 68
      viewInfo.isHidden = false
      lbContent.text =  document.summary ?? ""
      lbTitle.text = "Số hiệu     \(document.serialNumber ?? "")"
      widthImgTitle.constant = 0
      heightTitle.constant = (lbTitle.text != "" ? 28 : 0)
      lbTitleCode.text = "Từ ngày"
      lbTitleDate.text = "Đến ngày"
      lbCode.text = Constants.formatDate(document.fromDate) ?? "Chưa xác định"
      lbDate.text = Constants.formatDate(document.toDate) ?? "Chưa xác định"
    } else {
      heightUserInfo.constant = 0
      viewInfo.isHidden = true
      lbContent.text =  document.summary ?? ""
      if (document.sender != nil && document.sender! != "") {
        lbTitle.text = "  " + document.sender!
        widthImgTitle.constant = 20
      } else {
        lbTitle.text = ""
        widthImgTitle.constant = 0
      }
      
      heightTitle.constant = (lbTitle.text != "" ? 28 : 0)
      lbTitleCode.text = "Số hiệu"
      
        if(document.isInComing == true) {
      lbTitleDate.text = "Ngày đến"
        }
        else {
        lbTitleDate.text = "Ngày văn bản"
        }
        
      lbCode.text = document.serialNumber ?? ""
        //lbCode.font = lbCode.font.withSize(12)
      lbDate.text = Constants.formatDate(document.docDate) ?? ""
        lbDate.font = lbDate.font.withSize(12)
    }
    if(document.statusDocument != nil && document.statusDocument!.name != nil && document.statusDocument!.name! != "") {
      lbTag.text = "  \(document.statusDocument!.name!)  "
      lbTag.backgroundColor = UIColor.init(hexString: document.statusDocument!.colorCode ?? "#9E9E9E", transparency: 1)
    } else {
      lbTag.text = ""
      lbTag.backgroundColor = UIColor.clear
    }
    btComment.setTitle(" 0", for: .normal)
    
    if document.fileDocuments != nil {
      if document.fileDocuments!.count == 0 {
        btAttach.isHidden = true
      } else if document.fileDocuments!.count == 1 {
        btAttach.isHidden = false
        btAttach.setTitle(" Xem văn bản", for: .normal)
      } else {
        btAttach.isHidden = false
        btAttach.setTitle(" Xem \(document.fileDocuments!.count) văn bản", for: .normal)
      }
    } else {
      btAttach.isHidden = true
    }
    var color = Theme.default.normalTextColor
    lbContent.font = Theme.default.regularFont(size: 16)
    if (document.unReadColor != nil) {
      if let item = UIColor(hexString: document.unReadColor!){
        color = item
      }
      lbContent.font = Theme.default.boldFont(size: 16)
    }
    self.updateColor(color)    
    self.setupBubblePictures(document, color)
    if((document.fileDocuments == nil || document.fileDocuments!.count == 0) && (document.trackingDocuments == nil || document.trackingDocuments!.count == 0)){
      heightBTAttach.constant = 0
    } else {
      heightBTAttach.constant = 48
    }    
//    self.layoutIfNeeded()
        //highlight search text
        //Title
        let title = lbTitle.text ?? ""
        let attribute = NSMutableAttributedString(string: title)
        var range = (title as NSString).range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
        attribute.addAttributes([NSBackgroundColorAttributeName: UIColor.yellow], range: range)
        lbTitle.attributedText = attribute
        
        //code
        let code = lbCode.text ?? ""
        let attributeCode = NSMutableAttributedString(string: code)
        range = (code as NSString).range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
        attributeCode.addAttributes([NSBackgroundColorAttributeName: UIColor.yellow], range: range)
        lbCode.attributedText = attributeCode
        
        //Content
        let content = lbContent.text ?? ""
        let attributeContent = NSMutableAttributedString(string: content)
        range = (content as NSString).range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
        attributeContent.addAttributes([NSBackgroundColorAttributeName: UIColor.yellow], range: range)
        lbContent.attributedText = attributeContent
  }
  
  func updateColor(_ color: UIColor) {
//    lbName.textColor = color
//    lbPosition.textColor = color
    lbContent.textColor = color
//    imgTitle.image = #imageLiteral(resourceName: "ic_group").filled(withColor: color)
//    lbTitle.textColor = color
//    lbTitleCode.textColor = color
//    lbCode.textColor = color
//    lbTitleDate.textColor = color
//    lbDate.textColor = color
//    btComment.setTitleColor(color, for: .normal)
//    btComment.setImage(UIImage(named: "ic_comment")?.filled(withColor: color), for: .normal)
//    btAttach.setTitleColor(color, for: .normal)
//    //btAttach.setImage(UIImage(named: "ic_attach_blue")?.filled(withColor: color), for: .normal)
//    btAttach.borderColor = color
    
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return trackingAvatars.count
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 40, height: 40)
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
    var imgView = cell.viewWithTag(1) as? UIImageView
    if imgView == nil {
      imgView = UIImageView()
      imgView?.tag = 1
      imgView?.clipsToBounds = true
      imgView?.layer.cornerRadius = 20
      imgView?.layer.borderWidth = 0.5
      imgView?.layer.borderColor = UIColor.white.cgColor
      cell.addSubview(imgView!)
      imgView?.snp.makeConstraints({ (make) in
        make.left.top.bottom.right.equalToSuperview()
      })
    }
    let url = URL(string: trackingAvatars[indexPath.row])
    if url != nil {
      let resource = ImageResource(downloadURL: url!)
      imgView!.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, type, url) in
        if (image != nil) {
          imgView!.image = image!
        } else if let defaultImg: UIImage = UIImage(named: "ic_avatar_empty") {
          imgView?.image = defaultImg.imageResize(sizeChange: CGSize(width: 50, height: 50))
        } else {
          imgView!.image = UIImage(color: UIColor(hexString: "#E3E7F0")!, size: CGSize(width: 50, height: 50))
        }
      })
    } else {
      if let defaultImg: UIImage = UIImage(named: "ic_avatar_empty") {
        imgView?.image = defaultImg.imageResize(sizeChange: CGSize(width: 50, height: 50))
      } else {
        imgView!.image = UIImage(color: UIColor(hexString: "#E3E7F0")!, size: CGSize(width: 50, height: 50))
      }
    }
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func setupBubblePictures(_ document: Document, _ color: UIColor?) {
    if document.trackingDocuments != nil {
      if document.trackingDocuments!.count > 0 {
        var configFiles:[BPCellConfigFile] = []
        for i in 0..<document.trackingDocuments!.count {
          let url = URL(string: Constants.default.domainAddress + (document.trackingDocuments![i].assignToPicture ?? ""))
          if url != nil {
            let resource = ImageResource(downloadURL: url!)
            let imageView = UIImageView()
            imageView.tag = i
            imageView.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, type, url) in
              var imageTemp: UIImage!
              if(image != nil) {
                imageTemp = image!
              } else if let defaultImg = UIImage(named: "ic_avatar_empty") {
                imageTemp = defaultImg.imageResize(sizeChange: CGSize(width: 50, height: 50))
              } else {
                imageTemp =  UIImage(color: UIColor(hexString: "#E3E7F0")!, size: CGSize(width: 50, height: 50))
              }
              if (imageView.tag > configFiles.count) {
                configFiles.append(BPCellConfigFile(imageType: BPImageType.image(imageTemp), title: ""))
              } else {
                configFiles.insert(BPCellConfigFile(imageType: BPImageType.image(imageTemp), title: ""), at: imageView.tag)
              }
              self.loadBubblePictures(document, configFiles, color)
            })
          } else {
            var img = UIImage(color: UIColor(hexString: "#E3E7F0")!, size: CGSize(width: 50, height: 50))
            if let defaultImg = UIImage(named: "ic_avatar_empty") {
              img = defaultImg
            }
            configFiles.append(BPCellConfigFile(imageType: BPImageType.image(img), title: ""))
            self.loadBubblePictures(document, configFiles, color)
          }
        }        
      } else {
        self.loadBubblePictures(document, [])
        }
    } else {
        self.loadBubblePictures(document, [])
    }
  }
  
  func loadBubblePictures(_ document: Document, _ configFiles: [BPCellConfigFile], _ color: UIColor? = UIColor(hexString: "#4A90E2")) {
    var numberIcon = 3
    if (UIDevice.is3InchesIPHONE()||UIDevice.is4InchesIPHONE()) {
      numberIcon = 2
    }
    let layoutConfigurator = BPLayoutConfigurator(
      backgroundColorForTruncatedBubble: UIColor(hexString: "#E3E7F0")!,
      fontForBubbleTitles: Theme.default.regularFont(size: 18),
      colorForBubbleBorders: UIColor.white,
      colorForBubbleTitles: color!,
      maxCharactersForBubbleTitles: 1,
      maxNumberOfBubbles: numberIcon,
      widthForBubbleBorders: 0,
      distanceInterBubbles: -5,
      alignment: .left)

    if let trackingDocuments = document.trackingDocuments, trackingDocuments.count == configFiles.count {
      self.bubblePictures = BubblePictures(collectionView: self.collectionView, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
      self.bubblePictures.delegate = self
    }
  }
}

extension TaskTableCell: BPDelegate {
  
  func didSelectTruncatedBubble() {
    print("Selected truncated bubble")
  }
  
  func didSelectBubble(at index: Int) {
    print(index)
  }
  
}
