//
//  DetailPersonTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/17/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class DetailPersonTableViewCell: UITableViewCell {
  
  @IBOutlet weak var departmentLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var titleLabe: UILabel!
  @IBOutlet weak var processLabel: UILabel!
  @IBOutlet weak var contentLabel: UILabel!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var bulletImageView: UIImageView!
  @IBOutlet weak var borderProcessView: UIView!
    @IBOutlet weak var wrapperDepartmentView: UIView!
    
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .none
    // Initialization code
  }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        hideProgress(false)
    }
  
    func hideProgress(_ hide:Bool = true) {
    borderProcessView.isHidden = hide
    processLabel.isHidden = hide
  }
  
  func setStatusText(text: String?) {
    guard let textValue: String = text else {
      hideProgress()
      return
    }
    if textValue.lowercased() == "đã xem" {
      borderProcessView.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
      processLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    } else if textValue.lowercased() == "chưa xem" {
      borderProcessView.backgroundColor = UIColor.clear
      borderProcessView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
      borderProcessView.layer.borderWidth = 1.5
      processLabel.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    } else {
      hideProgress()
      return
    }
    processLabel.text = textValue
  }
  
  func setActive(active: Bool){
    if active {
      bulletImageView.image = #imageLiteral(resourceName: "ic_timeline_active")
    } else {
      bulletImageView.image = #imageLiteral(resourceName: "ic_timeline")
    }
  }
  
}
