//
//  NotificationTableCell.swift
//  TaskManagement
//
//  Created by Tuanhnm on 12/28/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import Kingfisher
import SwipeCellKit

class NotificationTableCell: SwipeTableViewCell {
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusView: UIButton!
    
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    iconImageView.layer.cornerRadius = iconImageView.widthf/2
    iconImageView.layer.masksToBounds = true
    
    statusView.layer.cornerRadius = 10
    statusView.layer.masksToBounds = true
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
  }
  
  func config(_ notification: NotificationItem) {
    if let desc: String = notification.summary {
      titleLabel.text = desc
    }
    if let status: String = notification.itemTypeName {
      statusView.setTitle(status, for: .normal)
    }
    
    if let avatarURLStr: String = notification.authorPicture,
        let avatarURL: URL = URL(string: Constants.default.domainAddress + avatarURLStr) {
        self.iconImageView.kf.setImage(with: avatarURL, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
    }
  }
  
}
