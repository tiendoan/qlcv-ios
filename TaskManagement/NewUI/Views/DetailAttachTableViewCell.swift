//
//  DetailAttachTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/18/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit

class DetailAttachTableViewCell: UITableViewCell {
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var sizeLabel: UILabel!
  
  @IBOutlet weak var closeButton: UIButton!
  
  var closeButtonTapped: (() -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  @IBAction func actionCloseButton(_ sender: UIButton){
    self.closeButtonTapped?()
  }
  
}
