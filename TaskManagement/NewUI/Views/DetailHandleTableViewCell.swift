//
//  DetailHandleTableViewCell.swift
//  TaskManagement
//
//  Created by Thang Nguyen on 9/13/17.
//  Copyright © 2017 Thang Nguyen. All rights reserved.
//

import UIKit
import SwipeCellKit

//enum DetailHandleStyle: Int {
//  case attachAndPerson = 1, list = 2, onlyAttach = 3, full = 4
//}

protocol DetailHandleTableViewCellDelegate {
    func expandAll(at: Int)
    func collapseAll(at: Int)
    func actionHistoryProcess(item: ProgressTracking)
    func onDetail(item: ProgressTracking)
    func backSubPage()
    func openFile(_ file: FileDocument)
}

class DetailHandleTableViewCell: UITableViewCell {
  
  @IBOutlet weak var tableView: UITableView!
  
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var jobTitleLabel: UILabel!
  @IBOutlet weak var sumaryLabel: UILabel!
  @IBOutlet weak var endTimeLabel: UILabel!
  @IBOutlet weak var leftLineView: UIView!
  @IBOutlet weak var statusView: UIView!
  @IBOutlet weak var statusDocumentLabel: UILabel!
  @IBOutlet weak var tableHeightLayoutConstraint: NSLayoutConstraint!
  //@IBOutlet weak var percentLabel: UILabel!
  
  var delegate: DetailHandleTableViewCellDelegate?
  var index = 0
  var isExpand = false
//  var style: DetailHandleStyle = .list {
//    didSet{
//      self.tableView.reloadData()
//    }
//  }
  var data: ProgressTracking?
    var arrChilds = [ProgressTracking]()
    
  override func awakeFromNib() {
    super.awakeFromNib()
    selectionStyle = .none
    tableView.register(withClass: DetailHandleTableViewCell.self)
    tableView.register(withClass: DetailAttachTableViewCell.self)
    tableView.register(withClass: DetailAssignPersonTableViewCell.self)
    tableView.register(withClass: DetailProcessItemCell.self)
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 95
    tableView.separatorStyle = .none
    // Initialization code
    
    tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
  }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = #imageLiteral(resourceName: "ic_avatar_empty")
        arrChilds = [ProgressTracking]()
        //self.tableHeightLayoutConstraint.constant = 0
        self.btnBack.isHidden = true
    }
    
    deinit {
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
  
//  override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
//    var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
//    size.height += tableView.contentSize.height
//    return size
//  }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let obj = object as? UITableView {
            if obj == self.tableView && keyPath == "contentSize" {
                if let newSize = change?[NSKeyValueChangeKey.newKey] as? CGSize {
                    //do stuff here
                    self.tableHeightLayoutConstraint.constant = newSize.height
                    self.layoutIfNeeded()
                }
            }
        }
    }
  
    func config(data: ProgressTracking?, isSubPage:Bool = false){
        self.btnBack.isHidden = !isSubPage
    self.data = data
    self.arrChilds = data?.recentChilds ?? [ProgressTracking]()
    tableView.isHidden = false
    if let avatar = data?.assignToPicture, let url = URL(string: Constants.default.domainAddress)?.appendingPathComponent(avatar){
        self.avatarImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "ic_avatar_empty"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    nameLabel.text = data?.assignToName ?? ""
    jobTitleLabel.text = data?.assignToJobTitle ?? ""
    sumaryLabel.text = data?.name ?? ""
    let startTime = Constants.formatDate(data?.fromDate, "dd 'Th'MM") ?? ""
    let endTime = Constants.formatDate(data?.toDate, "dd 'Th'MM") ?? ""
    endTimeLabel.text = startTime + " - " + endTime

    tableView.reloadData()
    if let status = data?.statusDocument {
      self.statusView.backgroundColor = UIColor(hexString: status.colorCode ?? "AEAEAE")
      self.statusDocumentLabel.text = status.name ?? "Chưa xác định"
    }
//    if let percent: Int = data?.percentFinish {
//      percentLabel.text = "\(percent.string)%"
//    }

    setNeedsLayout()
    layoutIfNeeded()
    self.tableHeightLayoutConstraint.constant = self.tableView.contentSize.height
  }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.delegate?.backSubPage()
    }
    
}

extension DetailHandleTableViewCell: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if !arrChilds.isEmpty {
        return isExpand ? 2 : 1
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
      case 1:
        return arrChilds.count
      default:
        return 1
    }
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch indexPath.section {
      case 0:
        let cell = tableView.dequeueReusableCell(withClass: DetailAssignPersonTableViewCell.self, for: indexPath)
        
        if let persons: [ProgressTracking] = data?.recentChilds,
            let attachments: [FileDocument] = data?.fileTrackingDocuments {
            cell.config(person: persons, files: attachments, expanded:isExpand) { [weak self] (expanded) in
                guard let sself = self else {
                    return
                }
                sself.isExpand = expanded
                if !expanded {
                    sself.delegate?.collapseAll(at: sself.index)
                } else {
                    sself.delegate?.expandAll(at: sself.index)
                }
            }
            
            cell.onOpenFiles = { [weak self] (file) in
                self?.delegate?.openFile(file)
            }
        }
        return cell
      default:
        let cell = tableView.dequeueReusableCell(withClass: DetailProcessItemCell.self, for: indexPath)
        cell.delegate = self
        if let item = data?.childs?[indexPath.row] {
            cell.config(data: item)
            cell.onDetailTapped = { [weak self] in
                self?.delegate?.onDetail(item:item)
            }
            cell.onOpenFiles = { [weak self] (file) in
                self?.delegate?.openFile(file)
            }
        }
        return cell
      }
  }
}

extension DetailHandleTableViewCell: UITableViewDelegate {
    func tableView (_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 48
        }
        
        return UITableViewAutomaticDimension
    }
}

extension DetailHandleTableViewCell: SwipeTableViewCellDelegate {
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
    if orientation == .right {
      let hitoryAction = SwipeAction(style: .default, title: "XEM LƯỢC SỬ") { action, indexPath in
        
        if let item = self.arrChilds[safe:indexPath.row] {
            self.delegate?.actionHistoryProcess(item: item)
        }
        
      }
      hitoryAction.image = #imageLiteral(resourceName: "ic_cell_view_history")
//      let doneAction = SwipeAction(style: .default, title: "HOÀN TẤT") { action, indexPath in
//
//      }
//      doneAction.image = #imageLiteral(resourceName: "ic_cell_done")
//      let moreAction = SwipeAction(style: .default, title: "KHÁC") { action, indexPath in
//
//      }
//      moreAction.image = #imageLiteral(resourceName: "ic_cell_more")
      let list = [hitoryAction]
      for item in list {
        item.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        item.textColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1)
        item.font = UIFont(name: "Muli-Bold", size: 10)
      }
      return list
    }
    else{
      return []
    }
  }
  
  func tableView (_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
    var options = SwipeTableOptions()
    options.expansionStyle = .selection
    options.transitionStyle = .drag
    options.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

    return options
  }
}
